package com.jeffrpowell.sevenwonders.game.algorithms;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jeffrpowell.sevenwonders.PlayerMeta;
import com.jeffrpowell.sevenwonders.ResourceType;
import com.jeffrpowell.sevenwonders.cards.Card;
import com.jeffrpowell.sevenwonders.cards.CardFactory;
import com.jeffrpowell.sevenwonders.game.Game;
import com.jeffrpowell.sevenwonders.wonders.WonderFactory;
import java.util.Map;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AlgorithmsTest{
	
	private Game.PrivateGameState gameState;
	private PlayerMeta me;
	private Game.PublicGameState publicState;
	private final Card lumberYard;
	private final Card stonePit;
	private final Card temple;
	private final Card loom;
	
	public AlgorithmsTest(){
		lumberYard = CardFactory.getCard("Lumber Yard", 3, 1);
		stonePit = CardFactory.getCard("Stone Pit", 3, 1);
		temple = CardFactory.getCard("Temple", 3, 2);
		loom = CardFactory.getCard("Loom", 3, 2);
	}
	
	@Before
	public void setUp(){
		me = new PlayerMeta("me");
		PlayerMeta left = new PlayerMeta("left");
		PlayerMeta right = new PlayerMeta("right");
		publicState = new Game.PublicGameState(me, left, right);
		Game.PublicGameState leftPublicState = new Game.PublicGameState(left, right, me);
		Game.PublicGameState rightPublicState = new Game.PublicGameState(right, me, left);
		Map<PlayerMeta, Game.PublicGameState> publicStates = Maps.newHashMap();
		publicStates.put(me, publicState);
		publicStates.put(left, leftPublicState);
		publicStates.put(right, rightPublicState);
		gameState = new Game.PrivateGameState(me, Lists.newArrayList(me, left, right), publicStates);
	}
	
	@Test
	public void testDefaultAssumptions(){
		Assert.assertTrue("Doesn't have correct starting gold amount", publicState.coins == 3);
	}
	
	//Change Game State Algorithms

	private void runAlgorithm(ChangeGameStateAlgorithm algorithm){
		algorithm.alterGameState(gameState);
	}
	
	@Test
	public void testNoOpChangeAlgorithm(){
		runAlgorithm(Algorithms.noOpChangeAlgorithm());
		testDefaultAssumptions();
	}

	@Test
	public void testCheaperResourcesAlgorithm_Material(){
		runAlgorithm(Algorithms.cheaperResourcesAlgorithm(true, true));
		Assert.assertTrue("Cheaper resources algorithm isn't applying", gameState.costPerLeftNeighborMaterial == 1);
		Assert.assertTrue("Cheaper resources algorithm is wrong", gameState.costPerRightNeighborMaterial == 2);
		Assert.assertTrue("Cheaper resources algorithm is wrong", gameState.costPerLeftNeighborGood == 2);
		Assert.assertTrue("Cheaper resources algorithm is wrong", gameState.costPerRightNeighborGood == 2);
	}
	
	@Test
	public void testCheaperResourcesAlgorithm_Good(){
		runAlgorithm(Algorithms.cheaperResourcesAlgorithm(false, false));
		Assert.assertTrue("Cheaper resources algorithm is wrong", gameState.costPerLeftNeighborMaterial == 2);
		Assert.assertTrue("Cheaper resources algorithm is wrong", gameState.costPerRightNeighborMaterial == 2);
		Assert.assertTrue("Cheaper resources algorithm isn't applying", gameState.costPerLeftNeighborGood == 1);
		Assert.assertTrue("Cheaper resources algorithm isn't applying", gameState.costPerRightNeighborGood == 1);
	}

	@Test
	public void testGainGoldAlgorithm_rawGold(){
		runAlgorithm(Algorithms.gainGoldAlgorithm(3, null, false));
		Assert.assertTrue("Didn't get right amount of gold", publicState.coins == 6);
	}

	@Test
	public void testGainGoldAlgorithm_myMaterial(){
		publicState.addBuiltCard(lumberYard);
		publicState.addBuiltCard(stonePit);
		runAlgorithm(Algorithms.gainGoldAlgorithm(3, Card.Type.MATERIAL, false));
		Assert.assertTrue("Didn't get right amount of gold", publicState.coins == 9);
	}

	@Test
	public void testGainGoldAlgorithm_neighborMaterial(){
		
		publicState.addBuiltCard(lumberYard);
		publicState.addBuiltCard(stonePit);
		gameState.getLeftNeighborState().addBuiltCard(lumberYard);
		gameState.getLeftNeighborState().addBuiltCard(stonePit);
		gameState.getRightNeighborState().addBuiltCard(lumberYard);
		gameState.getRightNeighborState().addBuiltCard(stonePit);
		runAlgorithm(Algorithms.gainGoldAlgorithm(3, Card.Type.MATERIAL, true));
		Assert.assertTrue("Didn't get right amount of gold", publicState.coins == 21);
	}

	@Test
	public void testGainGoldFromMyWonderStages(){
		publicState.wonder = WonderFactory.getWonder(true, "Babylon");
		publicState.buildNextWonderStage(lumberYard, gameState);
		publicState.buildNextWonderStage(stonePit, gameState);
		runAlgorithm(Algorithms.gainGoldFromMyWonderStages(3));
		Assert.assertTrue("Didn't get right amount of gold", publicState.coins == 9);
	}

	@Test
	public void testAdditionalResourcesAlgorithm_forum(){
		publicState.wonder = WonderFactory.getWonder(true, "Babylon");
		Assert.assertFalse("Shouldn't be able to cover goods cost", publicState.canCoverCost(Lists.newArrayList(ResourceType.GLASS)));
		runAlgorithm(Algorithms.additionalResourcesAlgorithm(true));
		Assert.assertTrue("Should be able to cover goods cost", publicState.canCoverCost(Lists.newArrayList(ResourceType.GLASS)));
	}

	@Test
	public void testAdditionalResourcesAlgorithm_caravansery(){
		publicState.wonder = WonderFactory.getWonder(true, "Alexandria");
		Assert.assertFalse("Should be able to cover material cost", publicState.canCoverCost(Lists.newArrayList(ResourceType.CLAY)));
		runAlgorithm(Algorithms.additionalResourcesAlgorithm(false));
		Assert.assertTrue("Should be able to cover material cost", publicState.canCoverCost(Lists.newArrayList(ResourceType.CLAY)));
	}

	@Test
	public void testAddRhodosShieldsAlgorithm(){
		Assert.assertTrue("No shields yet", publicState.getNumShields() == 0);
		runAlgorithm(Algorithms.addRhodosShieldsAlgorithm(2));
		Assert.assertTrue("Rhodos shields didn't apply", publicState.getNumShields() == 2);
	}

	@Test
	public void testEnableAlexandriaGoods(){
		publicState.wonder = WonderFactory.getWonder(true, "Babylon");
		Assert.assertFalse("Shouldn't be able to cover goods cost", publicState.canCoverCost(Lists.newArrayList(ResourceType.GLASS)));
		runAlgorithm(Algorithms.enableAlexandriaGoods());
		Assert.assertTrue("Should be able to cover goods cost", publicState.canCoverCost(Lists.newArrayList(ResourceType.GLASS)));
	}

	@Test
	public void testEnableAlexandriaMaterials(){
		publicState.wonder = WonderFactory.getWonder(true, "Alexandria");
		Assert.assertFalse("Should be able to cover material cost", publicState.canCoverCost(Lists.newArrayList(ResourceType.CLAY)));
		runAlgorithm(Algorithms.enableAlexandriaMaterials());
		Assert.assertTrue("Should be able to cover material cost", publicState.canCoverCost(Lists.newArrayList(ResourceType.CLAY)));
	}

//	@Test
//	public void testEnableBabylonSeventhPlay(){
//	}

//	@Test
//	public void testEnableOlympiaCloneGuild(){
//	}

	@Test
	public void testEnableOlympiaFreebies(){
		publicState.wonder = WonderFactory.getWonder(true, "Olympia");
		Assert.assertFalse("Shouldn't be able to cover goods cost", publicState.hasResourcesToBuildCard(temple));
		Assert.assertTrue("Should be able to cover goods cost", publicState.hasResourcesToBuildCard(loom));
		runAlgorithm(Algorithms.enableOlympiaFreebies());
		Assert.assertTrue("Should be able to cover goods cost", publicState.hasResourcesToBuildCard(temple));
		publicState.addBuiltCard(loom);
		Assert.assertTrue("Should be able to cover goods cost", publicState.hasResourcesToBuildCard(temple));
		publicState.addBuiltCard(temple);
		Assert.assertFalse("Shouldn't be able to cover goods cost", publicState.hasResourcesToBuildCard(temple));
	}

	//Victory Point Algorithms
	
//	@Test
//	public void testPointsForWonderStages(){
//	}
//
//	@Test
//	public void testPointsForMyCards(){
//	}
//
//	@Test
//	public void testPointsForNeighborsCards(){
//	}
//
//	@Test
//	public void testPointsForNeighborsMilitaryLosses(){
//	}
//
//	@Test
//	public void testEndGameMilitary(){
//	}
//
//	@Test
//	public void testEndGameTreasury(){
//	}
//
//	@Test
//	public void testEndGameWonder(){
//	}
//
//	@Test
//	public void testEndGameCivilian(){
//	}
//
//	@Test
//	public void testEndGameScientific(){
//	}
	
}
