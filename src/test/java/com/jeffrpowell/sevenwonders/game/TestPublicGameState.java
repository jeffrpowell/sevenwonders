package com.jeffrpowell.sevenwonders.game;

import com.jeffrpowell.sevenwonders.PlayerMeta;
import com.jeffrpowell.sevenwonders.ResourceType;
import com.jeffrpowell.sevenwonders.cards.CardFactory;
import com.jeffrpowell.sevenwonders.cards.ResourceCard;
import com.jeffrpowell.sevenwonders.game.Game.PublicGameState;
import com.jeffrpowell.sevenwonders.wonders.Wonder;
import com.jeffrpowell.sevenwonders.wonders.WonderFactory;
import jersey.repackaged.com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestPublicGameState{
	private PublicGameState state;
	private final Wonder wonder;
	public TestPublicGameState(){
		wonder = WonderFactory.getWonder(true, "Babylon"); //Produces a clay
	}
	
	@Before
	public void setUp(){
		PlayerMeta me = new PlayerMeta("me");
		PlayerMeta neighbor = new PlayerMeta("neighbor");
		state = new PublicGameState(me, neighbor, neighbor);
		state.wonder = wonder;
	}
	
	@Test
	public void testCanCoverCostUsingWonder(){
		Assert.assertTrue("Should be able to cover cost of one clay from wonder.", state.canCoverCost(Lists.newArrayList(ResourceType.CLAY)));
	}
	
	@Test
	public void testCanCoverCostUsingSimpleCard(){
		state.addBuiltCard(CardFactory.getCard("Lumber Yard", 3, 1));
		Assert.assertTrue("Should be able to cover cost of one lumber using Lumber Yard.", state.canCoverCost(Lists.newArrayList(ResourceType.LUMBER)));
	}
	
	@Test
	public void testCanCoverCostUsingNeighborsSimpleCard(){
		state.purchasedNeighborResources.get(state.leftNeighbor).add((ResourceCard)CardFactory.getCard("Lumber Yard", 3, 1));
		Assert.assertTrue("Should be able to cover cost of one lumber using neighbors Lumber Yard.", state.canCoverCost(Lists.newArrayList(ResourceType.LUMBER)));
	}
	
	@Test
	public void testCanCoverCostUsingWonderAndSimpleCard(){
		state.addBuiltCard(CardFactory.getCard("Lumber Yard", 3, 1));
		Assert.assertTrue("Should be able to cover cost of one lumber and one clay.", state.canCoverCost(Lists.newArrayList(ResourceType.LUMBER, ResourceType.CLAY)));
	}
	
	@Test
	public void testCanCoverCostMissingCard(){
		Assert.assertFalse("Shouldn't be able to cover cost of one lumber and one clay without card.", state.canCoverCost(Lists.newArrayList(ResourceType.LUMBER, ResourceType.CLAY)));
	}
	
	@Test
	public void testCanCoverCostUsingDoubleResourceCard(){
		state.addBuiltCard(CardFactory.getCard("Quarry", 3, 2));
		Assert.assertTrue("Should be able to cover cost of 2 stones with single Quarry card.", state.canCoverCost(Lists.newArrayList(ResourceType.STONE, ResourceType.STONE)));
	}
	
	@Test
	public void testCanCoverCostUsingSomeOfDoubleResourceCard(){
		state.addBuiltCard(CardFactory.getCard("Quarry", 3, 2));
		Assert.assertTrue("Should be able to cover cost of 2 stones with single Quarry card.", state.canCoverCost(Lists.newArrayList(ResourceType.STONE)));
	}
	
	@Test
	public void testCanCoverCostGoodsUsingForumCard(){
		state.possessesForum = true;
		Assert.assertTrue("Should be able to cover cost of 1 loom with single Forum card.", state.canCoverCost(Lists.newArrayList(ResourceType.LOOM)));
	}
	
	@Test
	public void testCanCoverCostUsingForumCardAndWonder(){
		state.possessesForum = true;
		Assert.assertTrue("Should be able to cover cost of 1 loom and 1 clay with single Forum card and wonder.", state.canCoverCost(Lists.newArrayList(ResourceType.LOOM, ResourceType.CLAY)));
	}
	
	@Test
	public void testCanCoverCostUsingCaravansaryCardAndWonder(){
		state.possessesCaravansery = true;
		Assert.assertTrue("Should be able to cover cost of 1 clay and 1 ore with single Caravansary card and wonder.", state.canCoverCost(Lists.newArrayList(ResourceType.ORE, ResourceType.CLAY)));
	}
	
	@Test
	public void testCanCoverCostComplexTrue(){
		//1 clay from wonder
		state.possessesCaravansery = true; //1 of any material (should choose ore)
		state.alexandriaGoods = true; //1 of any good (choose papyrus)
		state.addBuiltCard(CardFactory.getCard("Excavation", 4, 1)); //Stone / Clay (choose clay; if you choose stone, you can't do it)
		state.purchasedNeighborResources.get(state.leftNeighbor).add((ResourceCard)CardFactory.getCard("Timber Yard", 3, 1)); //Stone / Lumber (choose stone)
		Assert.assertTrue("Complex-true case failed.", state.canCoverCost(Lists.newArrayList(
			ResourceType.CLAY, 
			ResourceType.CLAY,
			ResourceType.ORE,
			ResourceType.STONE,
			ResourceType.PAPYRUS)));
	}
	
	@Test
	public void testCanCoverCostComplexFalse(){
		//1 clay from wonder
		state.possessesCaravansery = true; //1 of any material
		state.alexandriaGoods = true; //1 of any good
		state.addBuiltCard(CardFactory.getCard("Excavation", 4, 1)); //Stone / Clay
		state.purchasedNeighborResources.get(state.leftNeighbor).add((ResourceCard)CardFactory.getCard("Timber Yard", 3, 1)); //Stone / Lumber
		Assert.assertFalse("Complex-false case failed.", state.canCoverCost(Lists.newArrayList(
			ResourceType.CLAY, 
			ResourceType.CLAY,
			ResourceType.CLAY,
			ResourceType.ORE,
			ResourceType.STONE)));
	}
	
	@Test
	public void testCanCoverCostVariableTrue(){
		//1 clay from wonder
		state.addBuiltCard(CardFactory.getCard("Excavation", 4, 1)); //Stone / Clay (choose clay; if you choose stone, you can't do it)
		Assert.assertTrue("Complex-true case failed.", state.canCoverCost(Lists.newArrayList(ResourceType.CLAY, ResourceType.CLAY)));
	}
}
