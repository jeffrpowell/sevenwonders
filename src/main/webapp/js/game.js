jQuery(function () {
	$(document).ready(function ($) {
		var wsUri = "ws://" + document.location.host + "/7wonders/game";
		var websocket = new WebSocket(wsUri);
		var playerMeta = {};
		var selectedCard = {};
		var selectedPlayer = "";
		var gameState = {};
		var $selectCardModal = $('#selectCardModal');
		var $purchaseResourceModal = $('#purchaseResourceModal');
		var $scoreModal = $('#scoreModal');
		var $imageDetailModal = $('#imageDetailModal');
		var $wonderDetailModal = $('#wonderDetailModal');
		var $discardSearchModal = $('#discardSearchModal');
		var $chatPanel = $('#inGameChat');
		var $cardTemplate = "";
		var $wonderTemplate = "";
		var endOfGame = false;
		var newMessages = 0;
		
		$selectCardModal.modal({
			keyboard: false,
			backdrop: 'static',
			show: false
		});
		$purchaseResourceModal.modal({
			show: false
		});
		$scoreModal.modal({
			keyboard: false,
			backdrop: 'static',
			show: false
		});
		$imageDetailModal.modal({
			show: false
		});
		$wonderDetailModal.modal({
			show: false
		});
		$discardSearchModal.modal({
			keyboard: false,
			backdrop: 'static',
			show: false
		});
		
		$.get("partials/card.html", function(cardTemplate){
			$cardTemplate = cardTemplate;
		});
		
		$.get("partials/wonder.html", function(wonderTemplate){
			$wonderTemplate = wonderTemplate;
		});
		
		function sendMessage(data){
			//because I seem to always forget to stringify the data...
			if (!data.hasOwnProperty("player")){
				data.player = playerMeta;
			}
			websocket.send(JSON.stringify(data));
		}
		
		function submitCardPlay(play){
				if (selectedCard.name && selectedCard.numPlayers){
						sendMessage({id: "PLAY_CARD", card: selectedCard, play: play});
						$selectCardModal.modal('hide');
						selectedCard = {};
				}
		}
		
		$selectCardModal.find('#buildCard').click(function(){
			submitCardPlay("BUILD");
		});
		
		$selectCardModal.find('#wonderCard').click(function(){
			submitCardPlay("WONDER");
		});
		
		$selectCardModal.find('#discardCard').click(function(){
			submitCardPlay("DISCARD");
		});
		
		$selectCardModal.find('#cancelCard').click(function(){
			$selectCardModal.modal('hide');
			selectedCard = {};
		});
		
		$scoreModal.find('#okScore').click(function(){
			$scoreModal.modal('hide');
			sendMessage({id: 'PICK_UP_HAND'});
		});
		
		var returnToLobby = function(){
			setCookie("gameId", "", 0);
			document.location.href="lobby.html";
		};
		
		$scoreModal.find('#leaveScore').click(function(){
			returnToLobby();
		});
		
		$imageDetailModal.find('#okDetail').click(function(){
			$imageDetailModal.modal('hide');
		});
		
		$wonderDetailModal.find('#okWonder').click(function(){
			$wonderDetailModal.modal('hide');
		});
		
		$chatPanel.find(".panel-heading").click(function(){
				$chatPanel.toggleClass("panel-focused");
				if ($chatPanel.hasClass("panel-focused")){
						newMessages = 0;
						clearNewMessagesBadge();
				}
		});
		
		$chatPanel.find('input').keypress(function(event){
				if (event.which === 13 && $(this).val() !== ''){ //enter key
						sendMessage({msg: $(this).val(), id: "NEW_CHAT_MSG"});
						$(this).val("");
				}
		});
		
		function getPathToCardImage(age, name, numPlayers){
				return "http://" + document.location.host + "/7wonders/images/cards/age"+age+"/"+name.replace(/\s+/g, '')+"_"+numPlayers+".JPG";
		}
		
		function getPathToCardBackImage(age){
				return "http://" + document.location.host + "/7wonders/images/cards/backs/"+age+".JPG";
		}
		
		function getPathToWonderImage(shortName, sideA){
				var side = sideA ? "A" : "B";
				return "http://" + document.location.host + "/7wonders/images/wonder_boards/"+shortName+"_"+side+".JPG";
		}
		
		function getTokenImage(imageName){
				return "<img src='http://" + document.location.host + "/7wonders/images/misc/"+imageName+".PNG'>";
		}
		
		function drawCardInYourHand(card, willBePlayed){
				var $card = $('#hand').append($cardTemplate).find('.card:last');
				$card.addClass("fake-link");
				$card.data("age", card.age);
				$card.data("name", card.name);
				$card.data("numPlayers", card.numPlayers);
				$card.find('img').attr('src', getPathToCardImage(card.age, card.name, card.numPlayers));
				if (willBePlayed){
						$card.find('img').addClass('playing');
				}
				$card.click(function(){
					selectedCard = {age: $(this).data("age"), name: $(this).data("name"), numPlayers: $(this).data("numPlayers")};
					$selectCardModal.find('#selectedCardTitle').text(selectedCard.name);
					$selectCardModal.find('img').attr('src', getPathToCardImage(selectedCard.age, selectedCard.name, selectedCard.numPlayers));
					sendMessage({
						id: "PLAY_CARD",
						play: "SELECT", 
						card: selectedCard
					});
				});
		}
		
		function handleGameStateUpdate(){
			$('#hand').empty();
			//Fill out your hand of cards
			$.each(gameState.hand, function(index, value){
					drawCardInYourHand(value, false);
			});
			if (gameState.lastCardPlayed){
					drawCardInYourHand(gameState.lastCardPlayed, true);
			}
			redrawPlayArea();
		}
		
		//Fill out everything else laid out on the table (built cards, wonder board)
		function redrawPlayArea(){
			$('#wonder').empty();
		  var $wonderState = $('#wonder').append($wonderTemplate).find('.play-area');
		  $.each(gameState.publicStates[selectedPlayer].builtCards, function (type, cardList) {
					if (type === "MATERIAL" || type === "GOOD"){
							type = "RESOURCE";
					}
		  		var $container = $wonderState.find("#" + type);
		  		$.each(cardList, function (index, card) {
		  				var $builtCard = $container.append($cardTemplate).find('.card:last');
							$builtCard.data("age", card.age);
							$builtCard.data("name", card.name);
							$builtCard.data("numPlayers", card.numPlayers);
							$builtCard.addClass('fake-link');
							if ($container.find(".card").length > 1){
									$builtCard.addClass('card-stack');
							}
							$builtCard.find('img').attr('src', getPathToCardImage(card.age, card.name, card.numPlayers));
							$builtCard.click(function(){
							  $imageDetailModal.find('.modal-title').text($(this).data("name"));
								$imageDetailModal.find('img').attr('src', getPathToCardImage($(this).data("age"), $(this).data("name"), $(this).data("numPlayers")));
								$imageDetailModal.modal('show');
							});
		  		});
		  });
			var wonder = gameState.publicStates[selectedPlayer].wonder;
			$wonderState.find('#swipePlayAreaLeft').click(function(){
					selectedPlayer = gameState.publicStates[selectedPlayer].leftNeighbor.username;
					redrawPlayArea();
			});
			$wonderState.find('#swipePlayAreaRight').click(function(){
					selectedPlayer = gameState.publicStates[selectedPlayer].rightNeighbor.username;
					redrawPlayArea();
			});
			$wonderState.find('#selectedPlayer').text(selectedPlayer);
			$wonderState.find("#swipePlayAreaLeft").html("&larr; " + gameState.publicStates[selectedPlayer].leftNeighbor.username);
			$wonderState.find("#swipePlayAreaRight").html(gameState.publicStates[selectedPlayer].rightNeighbor.username + " &rarr;");
		  handlePurchaseResourcesButton($wonderState.find("#purchaseResources"));
			var coins = gameState.publicStates[selectedPlayer].coins;
			var coins3 = Math.floor(coins / 3);
			var coins1 = coins - (3*coins3);
			$wonderState.find("#playerCoins").html(getTokenImage("coin3_front") + " x"+coins3+" "+getTokenImage("coin1_front") + " x"+coins1);
			var combat = gameState.publicStates[selectedPlayer].conflictTokens;
			var conflictMap = {"-1": 0, 1: 0, 3: 0, 5: 0};
			$.each(combat, function(index, token){
				conflictMap[token]++;	
			});
			$wonderState.find("#playerConflict").html(
					getTokenImage("conflict1") + " x"+conflictMap[1]+
					getTokenImage("conflict3") + " x"+conflictMap[3]+
					getTokenImage("conflict5") + " x"+conflictMap[5]+
					getTokenImage("conflict-1") + " x"+conflictMap["-1"]
			);
			var $wonder = $wonderState.find('.wonder');
			$wonder.find('img').attr('src', getPathToWonderImage(wonder.shortName, wonder.sideA));
			$wonder.data('shortName', wonder.shortName);
			$wonder.data('sideA', wonder.sideA);
			$wonder.addClass('fake-link');
			$wonder.click(function(){
				$wonderDetailModal.find('.modal-title').text($(this).data("shortName"));
				$wonderDetailModal.find('img').attr('src', getPathToWonderImage($(this).data("shortName"), $(this).data("sideA")));
				$wonderDetailModal.modal('show');
			});
			var wonderStages = gameState.publicStates[selectedPlayer].wonder.stages;
			var $stages = $wonderState.find('.stages');
			$.each(wonderStages, function(index, stage){
				if (stage.spentCard){
					$stages.append("<div class='wonderstage stage-"+(index+1)+"-of-"+wonderStages.length+"'><img src='"+getPathToCardBackImage(stage.spentCard.age)+"'></div>");
				}	
			});	
		}
		
		function handlePurchaseResourcesButton($purchaseResourcesBtn){
			var myPublicState = gameState.publicStates[playerMeta.username];
			if (endOfGame){
				$purchaseResourcesBtn.removeClass('hidden');
				$purchaseResourcesBtn.text("Return to Lobby");
				$purchaseResourcesBtn.click(returnToLobby);
			}
			if ((myPublicState.leftNeighbor.username === selectedPlayer || myPublicState.rightNeighbor.username === selectedPlayer)
					&& myPublicState.coins - myPublicState.coinsSpentTrading > 0)
			{
				$purchaseResourcesBtn.removeClass('hidden');
				$purchaseResourcesBtn.click(function(){
						$purchaseResourceModal.find('#coinDisplay').text("You have "+(myPublicState.coins - myPublicState.coinsSpentTrading)+" coins");
						var $resourceOptions = $purchaseResourceModal.find('#resourceOptions');
						$resourceOptions.empty();
						var wonderResource = gameState.publicStates[selectedPlayer].wonder.resource;
						$resourceOptions.append("<tr><td>"+getTokenImage(wonderResource)+"</td>"+
												"<td><button type='button' class='btn btn-success wonder-resource' data-resource='"+wonderResource+"'>Buy</button>");
						$resourceOptions.find('button.wonder-resource').click(function(){
							sendMessage({id: "PURCHASE_RESOURCE", resource: $(this).data("resource"), seller: selectedPlayer});
							$purchaseResourceModal.modal('hide');
						});
						$.each(myPublicState.resourcesForSale[selectedPlayer], function(index, resourceCard){
							$resourceOptions.append("<tr><td>"+getResourceString(resourceCard)+"</td>"+
													"<td><button type='button' class='btn btn-success card-resource' "+
															"data-name='"+resourceCard.name+"' data-num-players='"+resourceCard.numPlayers+"' data-age='"+resourceCard.age+"'>Buy</button>");
						});
						$resourceOptions.find('button.card-resource').click(function(){
							sendMessage({id: "PURCHASE_RESOURCE", resource: {name: $(this).data("name"), numPlayers: $(this).data("num-players"), age: $(this).data("age")}, seller: selectedPlayer});
							$purchaseResourceModal.modal('hide');
						});
						$purchaseResourceModal.modal('show');
				});
			}
			else{
					$purchaseResourcesBtn.addClass('hidden');
			}
		}
		
		function getResourceString(resourceCard){
			var resourceStr = "";
			$.each(resourceCard.valuesGiven, function(resource, amount){
				for (var i = 0; i < amount; i++){
					resourceStr += getTokenImage(resource);
				}
			});
			return resourceStr;
		}
		
		function fillDiscardSearchModal(cardList){
				var $wrapper = $discardSearchModal.find('#discardSelect');
				$wrapper.empty();
				$.each(cardList, function(index, card){
						var $card = $wrapper.append($cardTemplate).find('.card:last');
						$card.addClass("fake-link");
						$card.data("age", card.age);
						$card.data("name", card.name);
						$card.data("numPlayers", card.numPlayers);
						$card.find('img').attr('src', getPathToCardImage(card.age, card.name, card.numPlayers));
						$card.click(function(){
							selectedCard = {age: $(this).data("age"), name: $(this).data("name"), numPlayers: $(this).data("numPlayers")};
							sendMessage({
								id: "PLAY_CARD",
								play: "BUILD", 
								card: selectedCard
							});
							$discardSearchModal.modal('hide');
						});
				});
				$discardSearchModal.modal('show');
		}
		
		function writeToScreen(message) {
				$('#wonder').prepend(message);
		}
		
		function clearNewMessagesBadge(){
				$chatPanel.find("#newMessages").hide();
		}
		
		function addNewMessageBadge(){
				if (!$chatPanel.hasClass("panel-focused")){
						$chatPanel.find("#newMessages").text(++newMessages).show();
				}
		}

		websocket.onerror = function (evt) {
			writeToScreen('<span style="color: red;">ERROR:</span> ' + evt.data);
		};
		
		websocket.onclose = function (close) {
			writeToScreen('You became disconnected from the server. Please refresh your browser.');
		};

		websocket.onopen = function (evt) {
			playerMeta.username = getCookie("username");
			playerMeta.sessionId = getCookie("sessionId");
			playerMeta.gameId = getCookie("gameId");
			selectedPlayer = playerMeta.username;
			sendMessage({id: 'HANDSHAKE'});
		};

		websocket.onmessage = function (evt) {
			var msg = JSON.parse(evt.data);
			if (msg.id === "GAME_STATE"){
				gameState = msg.gameState;
				handleGameStateUpdate();
			}
			else if (msg.id === "ERROR"){
					if (msg.msg === "EXCEPTION"){
							writeToScreen('<span style="color: red;">EXCEPTION:</span> A problem occurred on the server. Would you like to see the <button id="exceptionDetailsBtn" class="btn btn-link">details</button>?');
							$("#exceptionDetailsBtn").click(function(){
									writeToScreen(JSON.stringify(msg) + "<br>");
							});
					}
					else{
						alert('Error: ' + msg.msg);
				}
			}
			else if (msg.id === "CARD_PLAY_OPTIONS"){
				$selectCardModal.find('button').attr('disabled', 'disabled');
				$selectCardModal.find('#cancelCard').removeAttr('disabled');
				if (msg.collection.indexOf("BUILD") > -1){
						$selectCardModal.find('#buildCard').removeAttr('disabled');
				}
				if (msg.collection.indexOf("WONDER") > -1){
						$selectCardModal.find('#wonderCard').removeAttr('disabled');
				}
				if (msg.collection.indexOf("DISCARD") > -1){
						$selectCardModal.find('#discardCard').removeAttr('disabled');
				}
				$selectCardModal.modal('show');
			}
			else if (msg.id === "SEARCH_DISCARD"){
				  fillDiscardSearchModal(msg.collection);
			}
			else if (msg.id === "BABYLON_PLAY"){
				selectedCard = {age: msg.age, name: msg.name, numPlayers: msg.numPlayers};
				$selectCardModal.find('#selectedCardTitle').text("Would you like to also play " + selectedCard.name + "?");
				$selectCardModal.find('img').attr('src', getPathToCardImage(selectedCard.age, selectedCard.name, selectedCard.numPlayers));
				$selectCardModal.find('button').attr('disabled', 'disabled');
				var $nothingBtn = $selectCardModal.find('#nothingCard');
				if ($nothingBtn.length === 0){
						$selectCardModal.find('#cardOptions').append('<div class="row"><div class="col-xs-12"><button type="button" class="btn btn-default" id="nothingCard">Do Nothing</button></div></div>');
						$nothingBtn.click(function(){
								$selectCardModal.find('#nothingCard').addClass('hidden');
								submitCardPlay('NOTHING');
						});
				}
				else{
						$nothingBtn.removeClass('hidden');
				}
				if (msg.options.indexOf("BUILD") > -1){
						$selectCardModal.find('#buildCard').removeAttr('disabled');
				}
				if (msg.options.indexOf("WONDER") > -1){
						$selectCardModal.find('#wonderCard').removeAttr('disabled');
				}
				if (msg.options.indexOf("DISCARD") > -1){
						$selectCardModal.find('#discardCard').removeAttr('disabled');
				}
				$selectCardModal.modal('show');
			}
			else if (msg.id === "SCORE"){
				var $entryTable = $scoreModal.find('#scoreEntries');
				$entryTable.empty();
				$.each(msg.scorecard.scores, function(player, score){
						$entryTable.append("<tr><td>"+player+"</td><td>"+score+"</td></tr>");
				});
				if (msg.scorecard.finalScore){
					endOfGame = true;
					$scoreModal.find('.modal-title').text("Game Over");
					$scoreModal.find('.modal-body > p').text("Here are the final scores:");
					$scoreModal.find('#okScore').text("View Board");
					$scoreModal.find('#leaveScore').removeClass('hidden');
				}
				$scoreModal.modal('show');
			}
			else if (msg.id === "NEW_CHAT_MSG") {
				var $chatLog = $chatPanel.find("textarea");
				$chatLog.text("");
				if (msg.collection.length > 0) {
						var chat = "";
						$.each(msg.collection, function (index, line) {
								if (index > 0) {
										chat += "\n";
								}
								chat += line;
						});
						$chatLog.text(decodeURIComponent(chat));
						$chatLog.get(0).scrollTop = $chatLog.get(0).scrollHeight;
						addNewMessageBadge();
				}
	  	}
		};
	});
});