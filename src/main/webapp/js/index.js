jQuery(function($){
		function submitUsername(name){
				$.ajax("rest/session/" + name, {
				success: function (data, textStatus, jqXHR) {
						if (jqXHR.status === 204){
								$('p').text("That username is already in use by another player. Please select another.");
						}
						else if (jqXHR.status === 201){
								setCookie("username", data.player.username, 1);
								setCookie("sessionId", data.player.sessionId, 1);
								document.location.href="lobby.html";
						}
				}
			});
		}
		
		$('#username').keypress(function(event){
				if (event.which === 13 && $(this).val() !== ''){
						submitUsername($(this).val());
				}
		});
		
		$('#btnEnter').click(function(){
				if ($('#username').val() !== ""){
						submitUsername($('#username').val());
				}
		});
});