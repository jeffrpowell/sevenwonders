var wsUri = "ws://" + document.location.host + "/7wonders/socket";
var websocket = new WebSocket(wsUri);
var output = document.getElementById("output");

function writeToScreen(message) {
    output.innerHTML += message + "<br>";
}

websocket.onerror = function(evt) { 
		writeToScreen('<span style="color: red;">ERROR:</span> ' + evt.data);
};

websocket.onopen = function(evt) {
		writeToScreen("Connected to " + wsUri);
		for(var i = 0; i < 10; i++){
				websocket.send("Trying out message "+i);
		}
};

websocket.onmessage = function(evt) {
		writeToScreen(evt.data);
};