jQuery(function () {
	$(document).ready(function ($) {
		var wsUri = "ws://" + document.location.host + "/7wonders/lobby";
		var websocket = new WebSocket(wsUri);
		var output = document.getElementById("output");
		var playerMeta = {};
		var gameState = {};
		var $selectWonderModal = $('#selectWonderModal');
		
		$selectWonderModal.modal({
			keyboard: false,
			backdrop: 'static',
			show: false
		});
		
		function sendMessage(data){
			//because I seem to always forget to stringify the data...
			if (!data.hasOwnProperty("player")){
					data.player = playerMeta;
			}
			websocket.send(JSON.stringify(data));
		}

		function writeToScreen(message) {
			output.innerHTML = message;
		}
		
		function fillOutGameDetails(){
				$('#gameDiv h2').text(gameState.title);
				$('#playerList').empty();
			  var random = gameState.randomWonders;
				if (random){
						$('#wonderButton').addClass('hidden');
						$('#wonderChoice').prop('checked', false);
						$('#wonderChoiceRandom').prop('checked', true);
				}
				else{
						$('#wonderChoice').prop('checked', true);
						$('#wonderChoiceRandom').prop('checked', false);
						$('#wonderButton').removeClass('hidden');
				}
				$('#startGame, #wonderStrategy').addClass("hidden");
				var allPlayersSelectedAWonder = true;
				$.each(gameState.players, function(index, value){
					$('#playerList').append("<tr><td>"+(index+1)+"</td><td id='player"+index+"'></td><td id='wonder"+index+"'></td>");	
					$('#playerList #player'+index).text(value);
					var wonderText = random ? "Random" : gameState.wonderChoices[index];
					if (wonderText === ""){
							allPlayersSelectedAWonder = false;
					}
					$('#playerList #wonder'+index).text(wonderText);
					if (index === 0 && playerMeta.username === value){
						$('#startGame, #wonderStrategy').removeClass("hidden");
					}
					if (gameState.players.length < 3 || !allPlayersSelectedAWonder){
						$('#startGame').addClass("disabled").attr('disabled', 'disabled');
					}
					else{
						$('#startGame').removeClass("disabled").removeAttr('disabled');
					}
				});
				var $chatLog = $('#chatLog');
				$chatLog.text("");
				if (gameState.chatLog.length > 0){
						var chat = "";
						$.each(gameState.chatLog, function(index, line){
								if (index > 0){
										chat += "\n";
								}
								chat += line;
						});
						$chatLog.text(chat);
						$chatLog.get(0).scrollTop = $chatLog.get(0).scrollHeight;
				}
				$('#gameDiv').removeClass("hidden");
		}
		
		function removeGameDetails(){
				$('#gameDiv h2').text("");
				$('#playerList').empty();
				$('#chatLog').text("");
				$('#gameDiv').addClass("hidden");
		}

		websocket.onerror = function (evt) {
			writeToScreen('<span style="color: red;">ERROR:</span> ' + evt.data);
		};
		
		websocket.onclose = function (close) {
			writeToScreen('You became disconnected from the server. Please refresh your browser.');
		};

		websocket.onopen = function (evt) {
			playerMeta.username = getCookie("username");
			playerMeta.sessionId = getCookie("sessionId");
			sendMessage({id: 'HANDSHAKE'});
		};

		websocket.onmessage = function (evt) {
			var msg = JSON.parse(decodeURIComponent(evt.data));
			if (msg.id === "LOBBY_STATE"){
				$('#gameList').empty();
				$.each(msg.lobbyState.games, function(index, value){
					$('#gameList').append('<button id="btnGame'+index+'" type="button" class="join-game-button list-group-item list-group-item-warning"></button>');
					var btnGame = $('#gameList #btnGame'+index);
					btnGame.text(value);
					btnGame.data("gameTitle", value);
				});
				$('#gameList .join-game-button').click(function(){
						removeGameDetails();
						sendMessage({id: 'JOIN_GAME', msg: $(this).data().gameTitle});
				});
			}
			else if (msg.id === "NEW_GAME_STATE"){
				gameState = msg.gameState;
				fillOutGameDetails();
			}
			else if (msg.id === "ERROR"){
				writeToScreen('<span style="color: red;">ERROR:</span> ' + msg.msg);
			}
			else if (msg.id === "START_GAME"){
					setCookie("gameId", msg.player.gameId, 1);
					document.location.href="game.html";
			}
		};
		
		$('#newGameTitle').keypress(function(event){
			if (event.which === 13 && $(this).val() !== ''){
				removeGameDetails();
				sendMessage({msg: $(this).val(), id: "NEW_GAME"});
			}	
		});

		$('#createGame').click(function () {
				var title = $('#newGameTitle').val();
				if (title && title !== ''){
						removeGameDetails();
						sendMessage({msg: title, id: "NEW_GAME"});
				}
		});
		
		$('#chatInput').keypress(function(event){
				if (event.which === 13 && $(this).val() !== ''){ //enter key
						sendMessage({msg: $(this).val(), id: "NEW_CHAT_MSG"});
						$(this).val("");
				}
		});
		
		$('input[name="wonderChoice"]').change(function(){
				var randomWonders = $(this).val();
				sendMessage({msg: randomWonders, id: "RANDOM_WONDERS"});
		});

		$('#chooseWonder').click(function(){
				$selectWonderModal.modal('show');
		});
		
		$selectWonderModal.find('.lobbyWonder').click(function(){
				var wonder = $(this).data('wonder');
				sendMessage({msg: wonder, id: "SELECT_WONDER"});
				$selectWonderModal.modal('hide');
		});

		$('#leaveGame').click(function () {
				var title = $('#gameDiv h2').text();
				removeGameDetails();
				sendMessage({msg: title, id: "LEAVE_GAME"});
		});

		$('#startGame').click(function () {
				var title = $('#gameDiv h2').text();
				sendMessage({msg: title, id: "START_GAME"});
				//Don't redirect because validation has to happen server-side first. Redirect on receiving START_GAME message back
		});
	});
});