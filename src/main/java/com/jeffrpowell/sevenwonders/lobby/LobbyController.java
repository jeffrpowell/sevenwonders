package com.jeffrpowell.sevenwonders.lobby;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.jeffrpowell.sevenwonders.PlayerMeta;
import com.jeffrpowell.sevenwonders.SingleStringMessage;
import com.jeffrpowell.sevenwonders.game.GameSetup;
import com.jeffrpowell.sevenwonders.sockets.GameEndpoint;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class LobbyController{
	private final LobbyState lobbyState;
	private final Set<LobbyListener> listeners;
	private final GameManager gameManager;
	
	public LobbyController()
	{
		this.gameManager = new GameManager();
		this.lobbyState = new LobbyState();
		this.listeners = Sets.newHashSet();
	}
	
	public void addListener(LobbyListener listener)
	{
		listeners.add(listener);
	}
	
	public LobbyState getLobbyState()
	{
		return new LobbyState(lobbyState);
	}
	
	public void handleMessage(Map<String, Object> msg)
	{
		SingleStringMessage message = SingleStringMessage.fromMap(msg);
		switch (message.getId())
		{
		case NEW_GAME:
			createNewGame(message);
			break;
		case JOIN_GAME:
			addPlayerToGame(message);
			break;
		case LEAVE_GAME:
			removePlayerFromGame(message);
			break;
		case RANDOM_WONDERS:
			changeWonderChoiceStrategy(message);
			break;
		case SELECT_WONDER:
			selectWonder(message);
			break;
		case NEW_CHAT_MSG:
			addChatMessage(message);
			break;
		case START_GAME:
			startGame(message);
			break;
		}
	}
	
	private void createNewGame(SingleStringMessage msg)
	{
		if (gameManager.gameExists(msg.getMsg()))
		{
			sendErrorMessage(msg.getPlayer(), "A game with that title already exists.");
			return;
		}
		NewGameState playersCurrentGame = gameManager.getGame(msg.getPlayer());
		boolean gameAlsoRemoved = gameManager.removePlayerFromGame(msg.getPlayer()); //Remove the player from any game they are currently a part of
		if (gameAlsoRemoved){
			lobbyState.removeGame(playersCurrentGame.getTitle()); //relying on lobbyStateChanged() to be called eventually
		}
		NewGameState newGame = gameManager.createGame(msg.getMsg(), msg.getPlayer());
		lobbyState.addGame(newGame.getTitle());
		lobbyStateChanged();
		gameStateChanged(newGame);
	}
	
	private void addPlayerToGame(SingleStringMessage msg)
	{
		NewGameState game = gameManager.getGame(msg.getMsg());
		if (game.getPlayers().contains(msg.getPlayer().getUsername()))
		{
			//Player joining a game they're already a part of
			return;
		}
		if (game.getPlayers().size() == 7)
		{
			sendErrorMessage(msg.getPlayer(), "That game currently has the maximum number of players allowed.");
			return;
		}
		NewGameState playersCurrentGame = gameManager.getGame(msg.getPlayer());
		boolean gameAlsoRemoved = gameManager.removePlayerFromGame(msg.getPlayer()); //Remove the player from any game they are currently a part of
		if (gameAlsoRemoved){
			lobbyState.removeGame(playersCurrentGame.getTitle());
			lobbyStateChanged();
		}
		gameManager.addPlayer(game, msg.getPlayer());
		gameStateChanged(game);
	}
		
	public void changeWonderChoiceStrategy(SingleStringMessage message)
	{
		PlayerMeta player = message.getPlayer();
		NewGameState game = gameManager.getGame(player);
		game.setRandomWonders(Boolean.parseBoolean(message.getMsg()));
		gameStateChanged(game);
	}
		
	public void selectWonder(SingleStringMessage message)
	{
		PlayerMeta player = message.getPlayer();
		NewGameState game = gameManager.getGame(player);
		boolean notTaken = game.setWonderChoice(player.getUsername(), message.getMsg());
		if (!notTaken){
			sendErrorMessage(message.getPlayer(), message.getMsg() + " is already taken");
			return;
		}
		gameStateChanged(game);
	}
	
	public void removePlayerFromGame(PlayerMeta player){
		NewGameState playersCurrentGame = gameManager.getGame(player);
		boolean gameAlsoRemoved = gameManager.removePlayerFromGame(player); //Remove the player from any game they are currently a part of
		if (gameAlsoRemoved){
			lobbyState.removeGame(playersCurrentGame.getTitle());
			lobbyStateChanged();
		}
		else{
			gameStateChanged(playersCurrentGame);
		}
	}
	
	public void removePlayerFromGame(SingleStringMessage msg){
		removePlayerFromGame(msg.getPlayer());
	}
	
	public void addChatMessage(SingleStringMessage message)
	{
		PlayerMeta player = message.getPlayer();
		NewGameState game = gameManager.getGame(player);
		game.addChatLine(player.getUsername(), message.getMsg());
		gameStateChanged(game);
	}
	
	public void startGame(SingleStringMessage message)
	{
		NewGameState game = gameManager.getGame(message.getMsg());
		if (game.getPlayers().size() < 3)
		{
			sendErrorMessage(message.getPlayer(), "We don't support games with less than 3 players.");
			return;
		}
		else if (!game.isRandomWonders() && game.getWonderChoices().values().contains("")){
			sendErrorMessage(message.getPlayer(), "Not all players have selected a wonder yet.");
			return;
		}
		List<PlayerMeta> players = gameManager.startGame(game);
		lobbyState.removeGame(game.getTitle());
		int gameId = GameEndpoint.startNewGame(GameSetup.fromNewGameState(game, players));
		lobbyStateChanged();
		redirectToGame(players, gameId);
	}
	
	public void sendErrorMessage(PlayerMeta player, String error){
		for (LobbyListener listener : listeners){
			listener.error(player, error);
		}
	}

	public void lobbyStateChanged(){
		LobbyState copy = new LobbyState(lobbyState);
		for (LobbyListener listener : listeners){
			listener.lobbyStateChanged(copy);
		}
	}

	public void gameStateChanged(NewGameState game){
		NewGameState copy = new NewGameState(game);
		for (LobbyListener listener : listeners){
			listener.gameStateChanged(gameManager.getPlayers(copy), copy);
		}
	}
	
	public void redirectToGame(List<PlayerMeta> players, int gameId)
	{
		//First inject gameId into PlayerMeta
		List<PlayerMeta> newPlayers = Lists.newArrayList();
		for (PlayerMeta player : players){
			newPlayers.add(new PlayerMeta(player.getUsername(), player.getSessionId(), gameId));
		}
		
		for (LobbyListener listener : listeners){
			listener.gameRedirect(newPlayers);
		}
	}
}
