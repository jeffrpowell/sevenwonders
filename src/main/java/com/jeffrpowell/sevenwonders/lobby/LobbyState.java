package com.jeffrpowell.sevenwonders.lobby;

import java.util.ArrayList;
import java.util.List;

public class LobbyState {
	private final List<String> games;
	
	public LobbyState()
	{
		this.games = new ArrayList<>();
	}
	
	public LobbyState(LobbyState copy)
	{
		this.games = new ArrayList<>(copy.games);
	}
	
	public List<String> getGames()
	{
		return games;
	}
	
	public void addGame(String gameTitle)
	{
		games.add(gameTitle);
	}
	
	public void removeGame(String gameTitle)
	{
		games.remove(gameTitle);
	}
}
