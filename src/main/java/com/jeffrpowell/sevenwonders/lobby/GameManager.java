package com.jeffrpowell.sevenwonders.lobby;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jeffrpowell.sevenwonders.PlayerMeta;
import java.util.List;
import java.util.Map;

public class GameManager{
	private final Map<PlayerMeta, NewGameState> playerToGame;
	private final Map<String, NewGameState> titleToGame;
	private final Map<NewGameState, List<PlayerMeta>> gameToPlayers;

	public GameManager(){
		this.playerToGame = Maps.newHashMap();
		this.titleToGame = Maps.newHashMap();
		this.gameToPlayers = Maps.newHashMap();
	}

	public boolean gameExists(String gameTitle){
		return titleToGame.containsKey(gameTitle); 
	}
	
	public NewGameState createGame(String title, PlayerMeta firstPlayer){
		NewGameState newGame = new NewGameState(title);
		
		removePlayerFromGame(firstPlayer); //Remove the player from any game they are currently a part of
		titleToGame.put(newGame.getTitle(), newGame);
		gameToPlayers.put(newGame, Lists.<PlayerMeta>newArrayList());
		addPlayer(newGame, firstPlayer);
		return newGame;
	}
	
	public NewGameState getGame(PlayerMeta player){
		return playerToGame.get(player);
	}

	public NewGameState getGame(String gameTitle){
		return titleToGame.get(gameTitle);
	}
	
	public void addPlayer(NewGameState game, PlayerMeta player){
		if (!titleToGame.get(game.getTitle()).equals(game)){
			game = titleToGame.get(game.getTitle());
		}
		game.addPlayer(player.getUsername());
		playerToGame.put(player, game);
		gameToPlayers.get(game).add(player);
	}
	
	public List<PlayerMeta> getPlayers(NewGameState game){
		return gameToPlayers.get(game);
	}
	
	public List<PlayerMeta> startGame(NewGameState game){
		titleToGame.remove(game.getTitle());
		List<PlayerMeta> players = gameToPlayers.remove(game);
		for (PlayerMeta player : players){
			playerToGame.remove(player);
		}
		return players;
	}
	
	/**
	 * 
	 * @param player
	 * @return true if the game was also discarded, false otherwise
	 */
	public boolean removePlayerFromGame(PlayerMeta player)
	{
		if (playerToGame.containsKey(player)){
			NewGameState game = playerToGame.get(player);
			playerToGame.remove(player);
			gameToPlayers.get(game).remove(player);
			game.removePlayer(player.getUsername());
			return checkIfGameShouldBeRemoved(game);
		}
		return false;
	}
	
	private boolean checkIfGameShouldBeRemoved(NewGameState game)
	{
		if (game.getPlayers().isEmpty())
		{
			titleToGame.remove(game.getTitle());
			gameToPlayers.remove(game);
			return true;
		}
		return false;
	}
}
