package com.jeffrpowell.sevenwonders.lobby.messages;

import com.jeffrpowell.sevenwonders.Message;
import com.jeffrpowell.sevenwonders.Message.ID;
import com.jeffrpowell.sevenwonders.PlayerMeta;
import com.jeffrpowell.sevenwonders.lobby.LobbyState;

public class LobbyStateMessage extends Message{
	private static final ID id = ID.LOBBY_STATE;
	private final LobbyState lobbyState;
	public LobbyStateMessage(PlayerMeta player, LobbyState lobbyState)
	{
		super(id, player);
		this.lobbyState = lobbyState;
	}

	public LobbyState getLobbyState()
	{
		return lobbyState;
	}
}
