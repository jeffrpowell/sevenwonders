package com.jeffrpowell.sevenwonders.lobby.messages;

import com.jeffrpowell.sevenwonders.Message;
import com.jeffrpowell.sevenwonders.PlayerMeta;
import com.jeffrpowell.sevenwonders.lobby.NewGameState;

public class NewGameStateMessage extends Message{
	private final NewGameState gameState;
	public NewGameStateMessage(ID id, PlayerMeta player, NewGameState gameState)
	{
		super(id, player);
		this.gameState = gameState;
	}
	
	public static NewGameStateMessage creating(PlayerMeta player, NewGameState gameState)
	{
		return new NewGameStateMessage(ID.NEW_GAME, player, gameState);
	}
	
	public static NewGameStateMessage joining(PlayerMeta player, NewGameState gameState)
	{
		return new NewGameStateMessage(ID.JOIN_GAME, player, gameState);
	}
	
	public static NewGameStateMessage leaving(PlayerMeta player, NewGameState gameState)
	{
		return new NewGameStateMessage(ID.LEAVE_GAME, player, gameState);
	}
	
	public static NewGameStateMessage updating(PlayerMeta player, NewGameState gameState)
	{
		return new NewGameStateMessage(ID.NEW_GAME_STATE, player, gameState);
	}

	public NewGameState getGameState()
	{
		return gameState;
	}
}
