package com.jeffrpowell.sevenwonders.lobby;

import com.jeffrpowell.sevenwonders.PlayerMeta;
import java.util.List;

public interface LobbyListener {
	public void error(PlayerMeta player, String error);
	public void lobbyStateChanged(LobbyState state);
	public void gameStateChanged(List<PlayerMeta> player, NewGameState game);
	public void gameRedirect(List<PlayerMeta> players);
}
