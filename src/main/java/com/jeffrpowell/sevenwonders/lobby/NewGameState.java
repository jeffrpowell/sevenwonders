package com.jeffrpowell.sevenwonders.lobby;

import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class NewGameState {
	private final String title;
	private final List<String> players;
	private final List<String> chatLog;
	private final List<String> wonderChoices;
	private boolean randomWonders;
	
	public NewGameState(String title)
	{
		this.title = title;
		this.players = new ArrayList<>();
		this.chatLog = new ArrayList<>();
		this.wonderChoices = new ArrayList<>();
		this.randomWonders = true;
	}
	
	public NewGameState(NewGameState copy)
	{
		this.title = copy.title;
		this.players = new ArrayList<>(copy.players);
		this.chatLog = new ArrayList<>(copy.chatLog);
		this.wonderChoices = new ArrayList<>(copy.wonderChoices);
		this.randomWonders = copy.randomWonders;
	}
	
	public void addChatLine(String player, String text)
	{
		chatLog.add(player + ": " + text);
	}
	
	public void addPlayer(String player)
	{
		players.add(player);
		wonderChoices.add("");
	}
	
	public void removePlayer(String player)
	{
		int index = players.indexOf(player);
		players.remove(index);
		wonderChoices.remove(index);
	}
	
	public String getTitle()
	{
		return title;
	}
	
	public List<String> getPlayers()
	{
		return players;
	}
	
	public List<String> returnChatLog()
	{
		return chatLog;
	}
	
	public void setRandomWonders(boolean randomWonders){
		this.randomWonders = randomWonders;
	}
	
	public boolean isRandomWonders(){
		return randomWonders;
	}
	
	public boolean setWonderChoice(String player, String wonder){
		if (wonderChoices.contains(wonder)){
			return false;
		}
		wonderChoices.set(players.indexOf(player), wonder);
		return true;
	}
	
	public Map<String, String> getWonderChoices(){
		Map<String, String> wonderChoicesMap = Maps.newHashMap();
		for (int i = 0; i < players.size(); i++){
			wonderChoicesMap.put(players.get(i), wonderChoices.get(i));
		}
		return wonderChoicesMap;
	}

	@Override
	public int hashCode(){
		int hash = 7;
		hash = 47 * hash + Objects.hashCode(this.title);
		return hash;
	}

	@Override
	public boolean equals(Object obj){
		if (obj == null){
			return false;
		}
		if (getClass() != obj.getClass()){
			return false;
		}
		final NewGameState other = (NewGameState) obj;
		return Objects.equals(this.title, other.title);
	}
}
