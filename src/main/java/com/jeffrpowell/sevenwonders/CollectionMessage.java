package com.jeffrpowell.sevenwonders;

import com.google.gson.annotations.Expose;
import java.util.Collection;
import java.util.Map;

public class CollectionMessage extends Message{
	@Expose private final Collection collection;
	public CollectionMessage(ID id, PlayerMeta player, Collection collection)
	{
		super(id, player);
		this.collection = collection;
	}
	
	public Collection getCollection()
	{
		return collection;
	}
	
	public static CollectionMessage fromMap(Map<String, Object> map) throws IllegalArgumentException
	{
		Message meta = DefaultMessage.fromMap(map);
		Collection collection = (Collection)map.get("collection");
		if (collection == null)
		{
			throw new IllegalArgumentException("ArrayMessage.fromMap() requires a collection key.");
		}
		return new CollectionMessage(meta.getId(), meta.getPlayer(), collection);
	}
}
