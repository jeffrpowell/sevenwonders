package com.jeffrpowell.sevenwonders;

import com.google.gson.annotations.Expose;
import java.util.Map;

public class SingleStringMessage extends Message{
	@Expose private final String msg;
	public SingleStringMessage(ID id, PlayerMeta player, String msg)
	{
		super(id, player);
		this.msg = msg;
	}
	
	public String getMsg()
	{
		return msg;
	}
	
	public static SingleStringMessage fromMap(Map<String, Object> map) throws IllegalArgumentException
	{
		Message meta = DefaultMessage.fromMap(map);
		String msg = (String)map.get("msg");
		if (msg == null)
		{
			throw new IllegalArgumentException("SingleStringMessage.fromMap() requires a msg key.");
		}
		return new SingleStringMessage(meta.getId(), meta.getPlayer(), JsonUtils.urlEncode(msg));
	}
}
