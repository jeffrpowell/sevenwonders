package com.jeffrpowell.sevenwonders;

public enum ResourceType {
	CLAY, LUMBER, STONE, ORE, PAPYRUS, LOOM, GLASS, COIN
}
