package com.jeffrpowell.sevenwonders.cards;

import com.jeffrpowell.sevenwonders.ResourceType;
import java.util.List;

public class CivilianCard extends Card{
	private static final Card.Type type = Card.Type.CIVILIAN;
	private final int victoryPoints;

	public CivilianCard(String name, int numPlayers, int age, List<ResourceType> cost, int victoryPoints){
		super(name, type, numPlayers, age, cost);
		this.victoryPoints = victoryPoints;
	}

	public int getVictoryPoints(){
		return victoryPoints;
	}
}
