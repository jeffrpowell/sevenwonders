package com.jeffrpowell.sevenwonders.cards;

import com.google.common.collect.Maps;
import com.google.gson.annotations.Expose;
import com.jeffrpowell.sevenwonders.ResourceType;
import java.util.List;
import java.util.Map;

public class ResourceCard extends Card{
	@Expose private final Map<ResourceType, Integer> valuesGiven;

	public ResourceCard(String name, Type type, int numPlayers, int age, List<ResourceType> cost, ResourceType... resources){
		super(name, type, numPlayers, age, cost);
		this.valuesGiven = Maps.newHashMap();
		for (ResourceType resource : resources){
			if (!valuesGiven.containsKey(resource))
			{
				valuesGiven.put(resource, 0);
			}
			valuesGiven.put(resource, valuesGiven.get(resource)+1);
		}
	}

	public Map<ResourceType, Integer> getValuesGiven(){
		return Maps.newHashMap(valuesGiven);
	}
}
