package com.jeffrpowell.sevenwonders.cards;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import static com.jeffrpowell.sevenwonders.ResourceType.CLAY;
import static com.jeffrpowell.sevenwonders.ResourceType.COIN;
import static com.jeffrpowell.sevenwonders.ResourceType.GLASS;
import static com.jeffrpowell.sevenwonders.ResourceType.LOOM;
import static com.jeffrpowell.sevenwonders.ResourceType.LUMBER;
import static com.jeffrpowell.sevenwonders.ResourceType.ORE;
import static com.jeffrpowell.sevenwonders.ResourceType.PAPYRUS;
import static com.jeffrpowell.sevenwonders.ResourceType.STONE;
import static com.jeffrpowell.sevenwonders.cards.ScientificCard.Symbol.COMPASS;
import static com.jeffrpowell.sevenwonders.cards.ScientificCard.Symbol.GEAR;
import static com.jeffrpowell.sevenwonders.cards.ScientificCard.Symbol.TABLET;
import com.jeffrpowell.sevenwonders.game.algorithms.Algorithms;
import com.jeffrpowell.sevenwonders.game.algorithms.VictoryPointCountingAlgorithm;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class CardFactory {
	private static final List<Card> cards;
	private static final Map<String, Map<Integer, Card>> cardIndex;
	private static final Map<String, Map<Integer, Map<Integer, Card>>> goodsIndex;
	private static final List<Card> guildCards;
	
	private static void addDescendantsToEachAndReverse(String cardName, String...descendantNames)
	{
		List<Card> descendants = Lists.newArrayList();
		for (String descendantName : descendantNames)
		{
			descendants.addAll(cardIndex.get(descendantName).values());
		}
		List<Card> ancestors = Lists.newArrayList(cardIndex.get(cardName).values());
		for (Card targetCard : ancestors)
		{
			targetCard.addDescendants(descendants);
		}
		for (Card descendant : descendants)
		{
			descendant.addAncestors(ancestors);
		}
	}
	static{
		cards = Lists.newArrayList();
		int age = 1;
		cards.add(new ResourceCard("Lumber Yard", Card.Type.MATERIAL, 3, age, Collections.EMPTY_LIST, LUMBER));
		cards.add(new ResourceCard("Lumber Yard", Card.Type.MATERIAL, 4, age, Collections.EMPTY_LIST, LUMBER));
		cards.add(new ResourceCard("Stone Pit", Card.Type.MATERIAL, 3, age, Collections.EMPTY_LIST, STONE));
		cards.add(new ResourceCard("Stone Pit", Card.Type.MATERIAL, 5, age, Collections.EMPTY_LIST, STONE));
		cards.add(new ResourceCard("Clay Pool", Card.Type.MATERIAL, 3, age, Collections.EMPTY_LIST, CLAY));
		cards.add(new ResourceCard("Clay Pool", Card.Type.MATERIAL, 5, age, Collections.EMPTY_LIST, CLAY));
		cards.add(new ResourceCard("Ore Vein", Card.Type.MATERIAL, 3, age, Collections.EMPTY_LIST, ORE));
		cards.add(new ResourceCard("Ore Vein", Card.Type.MATERIAL, 4, age, Collections.EMPTY_LIST, ORE));
		cards.add(new ResourceCard("Tree Farm", Card.Type.MATERIAL, 6, age, Lists.newArrayList(COIN), LUMBER, CLAY));
		cards.add(new ResourceCard("Excavation", Card.Type.MATERIAL, 4, age, Lists.newArrayList(COIN), STONE, CLAY));
		cards.add(new ResourceCard("Clay Pit", Card.Type.MATERIAL, 3, age, Lists.newArrayList(COIN), CLAY, ORE));
		cards.add(new ResourceCard("Timber Yard", Card.Type.MATERIAL, 3, age, Lists.newArrayList(COIN), STONE, LUMBER));
		cards.add(new ResourceCard("Forest Cave", Card.Type.MATERIAL, 5, age, Lists.newArrayList(COIN), LUMBER, ORE));
		cards.add(new ResourceCard("Mine", Card.Type.MATERIAL, 6, age, Lists.newArrayList(COIN), ORE, STONE));
		cards.add(new ResourceCard("Loom", Card.Type.GOOD, 3, age, Collections.EMPTY_LIST, LOOM));
		cards.add(new ResourceCard("Loom", Card.Type.GOOD, 6, age, Collections.EMPTY_LIST, LOOM));
		cards.add(new ResourceCard("Glassworks", Card.Type.GOOD, 3, age, Collections.EMPTY_LIST, GLASS));
		cards.add(new ResourceCard("Glassworks", Card.Type.GOOD, 6, age, Collections.EMPTY_LIST, GLASS));
		cards.add(new ResourceCard("Press", Card.Type.GOOD, 3, age, Collections.EMPTY_LIST, PAPYRUS));
		cards.add(new ResourceCard("Press", Card.Type.GOOD, 6, age, Collections.EMPTY_LIST, PAPYRUS));
		cards.add(new CivilianCard("Pawnshop", 4, age, Collections.EMPTY_LIST, 3));
		cards.add(new CivilianCard("Pawnshop", 7, age, Collections.EMPTY_LIST, 3));
		cards.add(new CivilianCard("Baths", 3, age, Lists.newArrayList(STONE), 3));
		cards.add(new CivilianCard("Baths", 7, age, Lists.newArrayList(STONE), 3));
		cards.add(new CivilianCard("Altar", 3, age, Collections.EMPTY_LIST, 2));
		cards.add(new CivilianCard("Altar", 5, age, Collections.EMPTY_LIST, 2));
		cards.add(new CivilianCard("Theater", 3, age, Collections.EMPTY_LIST, 2));
		cards.add(new CivilianCard("Theater", 6, age, Collections.EMPTY_LIST, 2));
		cards.add(new MilitaryCard("Stockade", 3, age, Lists.newArrayList(LUMBER), 1));
		cards.add(new MilitaryCard("Stockade", 7, age, Lists.newArrayList(LUMBER), 1));
		cards.add(new MilitaryCard("Barracks", 3, age, Lists.newArrayList(ORE), 1));
		cards.add(new MilitaryCard("Barracks", 5, age, Lists.newArrayList(ORE), 1));
		cards.add(new MilitaryCard("Guard Tower", 3, age, Lists.newArrayList(CLAY), 1));
		cards.add(new MilitaryCard("Guard Tower", 4, age, Lists.newArrayList(CLAY), 1));
		cards.add(new ScientificCard("Apothecary", 3, age, Lists.newArrayList(LOOM), COMPASS));
		cards.add(new ScientificCard("Apothecary", 5, age, Lists.newArrayList(LOOM), COMPASS));
		cards.add(new ScientificCard("Workshop", 3, age, Lists.newArrayList(GLASS), GEAR));
		cards.add(new ScientificCard("Workshop", 7, age, Lists.newArrayList(GLASS), GEAR));
		cards.add(new ScientificCard("Scriptorium", 3, age, Lists.newArrayList(PAPYRUS), TABLET));
		cards.add(new ScientificCard("Scriptorium", 4, age, Lists.newArrayList(PAPYRUS), TABLET));
		cards.add(new CommercialCard("Tavern", 4, age, Collections.EMPTY_LIST, Algorithms.gainGoldAlgorithm(5, null, false)));
		cards.add(new CommercialCard("Tavern", 5, age, Collections.EMPTY_LIST, Algorithms.gainGoldAlgorithm(5, null, false)));
		cards.add(new CommercialCard("Tavern", 7, age, Collections.EMPTY_LIST, Algorithms.gainGoldAlgorithm(5, null, false)));
		cards.add(new CommercialCard("East Trading Post", 3, age, Collections.EMPTY_LIST, Algorithms.cheaperResourcesAlgorithm(true, false)));
		cards.add(new CommercialCard("East Trading Post", 7, age, Collections.EMPTY_LIST, Algorithms.cheaperResourcesAlgorithm(true, false)));
		cards.add(new CommercialCard("West Trading Post", 3, age, Collections.EMPTY_LIST, Algorithms.cheaperResourcesAlgorithm(true, true)));
		cards.add(new CommercialCard("West Trading Post", 7, age, Collections.EMPTY_LIST, Algorithms.cheaperResourcesAlgorithm(true, true)));
		cards.add(new CommercialCard("Marketplace", 3, age, Collections.EMPTY_LIST, Algorithms.cheaperResourcesAlgorithm(false, true)));
		cards.add(new CommercialCard("Marketplace", 6, age, Collections.EMPTY_LIST, Algorithms.cheaperResourcesAlgorithm(false, true)));
		age = 2;
		cards.add(new ResourceCard("Sawmill", Card.Type.MATERIAL, 3, age, Lists.newArrayList(COIN), LUMBER, LUMBER));
		cards.add(new ResourceCard("Sawmill", Card.Type.MATERIAL, 4, age, Lists.newArrayList(COIN), LUMBER, LUMBER));
		cards.add(new ResourceCard("Quarry", Card.Type.MATERIAL, 3, age, Lists.newArrayList(COIN), STONE, STONE));
		cards.add(new ResourceCard("Quarry", Card.Type.MATERIAL, 4, age, Lists.newArrayList(COIN), STONE, STONE));
		cards.add(new ResourceCard("Brickyard", Card.Type.MATERIAL, 3, age, Lists.newArrayList(COIN), CLAY, CLAY));
		cards.add(new ResourceCard("Brickyard", Card.Type.MATERIAL, 4, age, Lists.newArrayList(COIN), CLAY, CLAY));
		cards.add(new ResourceCard("Foundry", Card.Type.MATERIAL, 3, age, Lists.newArrayList(COIN), ORE, ORE));
		cards.add(new ResourceCard("Foundry", Card.Type.MATERIAL, 4, age, Lists.newArrayList(COIN), ORE, ORE));
		cards.add(new ResourceCard("Loom", Card.Type.GOOD, 3, age, Collections.EMPTY_LIST, LOOM));
		cards.add(new ResourceCard("Loom", Card.Type.GOOD, 5, age, Collections.EMPTY_LIST, LOOM));
		cards.add(new ResourceCard("Glassworks", Card.Type.GOOD, 3, age, Collections.EMPTY_LIST, GLASS));
		cards.add(new ResourceCard("Glassworks", Card.Type.GOOD, 5, age, Collections.EMPTY_LIST, GLASS));
		cards.add(new ResourceCard("Press", Card.Type.GOOD, 3, age, Collections.EMPTY_LIST, PAPYRUS));
		cards.add(new ResourceCard("Press", Card.Type.GOOD, 5, age, Collections.EMPTY_LIST, PAPYRUS));
		cards.add(new CivilianCard("Aqueduct", 3, age, Lists.newArrayList(STONE, STONE, STONE), 5));
		cards.add(new CivilianCard("Aqueduct", 7, age, Lists.newArrayList(STONE, STONE, STONE), 5));
		cards.add(new CivilianCard("Temple", 3, age, Lists.newArrayList(LUMBER, CLAY, GLASS), 3));
		cards.add(new CivilianCard("Temple", 6, age, Lists.newArrayList(LUMBER, CLAY, GLASS), 3));
		cards.add(new CivilianCard("Statue", 3, age, Lists.newArrayList(LUMBER, ORE, ORE), 4));
		cards.add(new CivilianCard("Statue", 7, age, Lists.newArrayList(LUMBER, ORE, ORE), 4));
		cards.add(new CivilianCard("Courthouse", 3, age, Lists.newArrayList(CLAY, CLAY, LOOM), 4));
		cards.add(new CivilianCard("Courthouse", 5, age, Lists.newArrayList(CLAY, CLAY, LOOM), 4));
		cards.add(new MilitaryCard("Walls", 3, age, Lists.newArrayList(STONE, STONE, STONE), 2));
		cards.add(new MilitaryCard("Walls", 7, age, Lists.newArrayList(STONE, STONE, STONE), 2));
		cards.add(new MilitaryCard("Training Ground", 4, age, Lists.newArrayList(LUMBER, ORE, ORE), 2));
		cards.add(new MilitaryCard("Training Ground", 6, age, Lists.newArrayList(LUMBER, ORE, ORE), 2));
		cards.add(new MilitaryCard("Training Ground", 7, age, Lists.newArrayList(LUMBER, ORE, ORE), 2));
		cards.add(new MilitaryCard("Stables", 3, age, Lists.newArrayList(ORE, CLAY, LUMBER), 2));
		cards.add(new MilitaryCard("Stables", 5, age, Lists.newArrayList(ORE, CLAY, LUMBER), 2));
		cards.add(new MilitaryCard("Archery Range", 3, age, Lists.newArrayList(LUMBER, LUMBER, ORE), 2));
		cards.add(new MilitaryCard("Archery Range", 6, age, Lists.newArrayList(LUMBER, LUMBER, ORE), 2));
		cards.add(new ScientificCard("Dispensary", 3, age, Lists.newArrayList(ORE, ORE, GLASS), COMPASS));
		cards.add(new ScientificCard("Dispensary", 4, age, Lists.newArrayList(ORE, ORE, GLASS), COMPASS));
		cards.add(new ScientificCard("Laboratory", 3, age, Lists.newArrayList(CLAY, CLAY, PAPYRUS), GEAR));
		cards.add(new ScientificCard("Laboratory", 5, age, Lists.newArrayList(CLAY, CLAY, PAPYRUS), GEAR));
		cards.add(new ScientificCard("Library", 3, age, Lists.newArrayList(STONE, STONE, LOOM), TABLET));
		cards.add(new ScientificCard("Library", 6, age, Lists.newArrayList(STONE, STONE, LOOM), TABLET));
		cards.add(new ScientificCard("School", 3, age, Lists.newArrayList(LUMBER, PAPYRUS), TABLET));
		cards.add(new ScientificCard("School", 7, age, Lists.newArrayList(LUMBER, PAPYRUS), TABLET));
		cards.add(new CommercialCard("Forum", 3, age, Lists.newArrayList(CLAY, CLAY), Algorithms.additionalResourcesAlgorithm(true)));
		cards.add(new CommercialCard("Forum", 6, age, Lists.newArrayList(CLAY, CLAY), Algorithms.additionalResourcesAlgorithm(true)));
		cards.add(new CommercialCard("Forum", 7, age, Lists.newArrayList(CLAY, CLAY), Algorithms.additionalResourcesAlgorithm(true)));
		cards.add(new CommercialCard("Caravansery", 3, age, Lists.newArrayList(LUMBER, LUMBER), Algorithms.additionalResourcesAlgorithm(false)));
		cards.add(new CommercialCard("Caravansery", 5, age, Lists.newArrayList(LUMBER, LUMBER), Algorithms.additionalResourcesAlgorithm(false)));
		cards.add(new CommercialCard("Caravansery", 6, age, Lists.newArrayList(LUMBER, LUMBER), Algorithms.additionalResourcesAlgorithm(false)));
		cards.add(new CommercialCard("Vineyard", 3, age, Collections.EMPTY_LIST, Algorithms.gainGoldAlgorithm(1, Card.Type.MATERIAL, true)));
		cards.add(new CommercialCard("Vineyard", 6, age, Collections.EMPTY_LIST, Algorithms.gainGoldAlgorithm(1, Card.Type.MATERIAL, true)));
		cards.add(new CommercialCard("Bazar", 4, age, Collections.EMPTY_LIST, Algorithms.gainGoldAlgorithm(2, Card.Type.GOOD, true)));
		cards.add(new CommercialCard("Bazar", 7, age, Collections.EMPTY_LIST, Algorithms.gainGoldAlgorithm(2, Card.Type.GOOD, true)));
		age = 3;
		cards.add(new CivilianCard("Pantheon", 3, age, Lists.newArrayList(CLAY, CLAY, ORE, PAPYRUS, LOOM, GLASS), 7));
		cards.add(new CivilianCard("Pantheon", 6, age, Lists.newArrayList(CLAY, CLAY, ORE, PAPYRUS, LOOM, GLASS), 7));
		cards.add(new CivilianCard("Gardens", 3, age, Lists.newArrayList(LUMBER, CLAY, CLAY), 5));
		cards.add(new CivilianCard("Gardens", 4, age, Lists.newArrayList(LUMBER, CLAY, CLAY), 5));
		cards.add(new CivilianCard("Town Hall", 3, age, Lists.newArrayList(GLASS, ORE, STONE, STONE), 6));
		cards.add(new CivilianCard("Town Hall", 5, age, Lists.newArrayList(GLASS, ORE, STONE, STONE), 6));
		cards.add(new CivilianCard("Town Hall", 6, age, Lists.newArrayList(GLASS, ORE, STONE, STONE), 6));
		cards.add(new CivilianCard("Palace", 3, age, Lists.newArrayList(GLASS, PAPYRUS, LOOM, CLAY, LUMBER, ORE, STONE), 8));
		cards.add(new CivilianCard("Palace", 7, age, Lists.newArrayList(GLASS, PAPYRUS, LOOM, CLAY, LUMBER, ORE, STONE), 8));
		cards.add(new CivilianCard("Senate", 3, age, Lists.newArrayList(ORE, STONE, LUMBER, LUMBER), 6));
		cards.add(new CivilianCard("Senate", 5, age, Lists.newArrayList(ORE, STONE, LUMBER, LUMBER), 6));
		cards.add(new MilitaryCard("Fortifications", 3, age, Lists.newArrayList(STONE, ORE, ORE, ORE), 3));
		cards.add(new MilitaryCard("Fortifications", 7, age, Lists.newArrayList(STONE, ORE, ORE, ORE), 3));
		cards.add(new MilitaryCard("Circus", 4, age, Lists.newArrayList(STONE, STONE, STONE, ORE), 3));
		cards.add(new MilitaryCard("Circus", 5, age, Lists.newArrayList(STONE, STONE, STONE, ORE), 3));
		cards.add(new MilitaryCard("Circus", 6, age, Lists.newArrayList(STONE, STONE, STONE, ORE), 3));
		cards.add(new MilitaryCard("Arsenal", 3, age, Lists.newArrayList(ORE, LUMBER, LUMBER, LOOM), 3));
		cards.add(new MilitaryCard("Arsenal", 4, age, Lists.newArrayList(ORE, LUMBER, LUMBER, LOOM), 3));
		cards.add(new MilitaryCard("Arsenal", 7, age, Lists.newArrayList(ORE, LUMBER, LUMBER, LOOM), 3));
		cards.add(new MilitaryCard("Siege Workshop", 3, age, Lists.newArrayList(LUMBER, CLAY, CLAY, CLAY), 3));
		cards.add(new MilitaryCard("Siege Workshop", 5, age, Lists.newArrayList(LUMBER, CLAY, CLAY, CLAY), 3));
		cards.add(new ScientificCard("Lodge", 3, age, Lists.newArrayList(CLAY, CLAY, LOOM, PAPYRUS), COMPASS));
		cards.add(new ScientificCard("Lodge", 6, age, Lists.newArrayList(CLAY, CLAY, LOOM, PAPYRUS), COMPASS));
		cards.add(new ScientificCard("Observatory", 3, age, Lists.newArrayList(ORE, ORE, GLASS, LOOM), GEAR));
		cards.add(new ScientificCard("Observatory", 7, age, Lists.newArrayList(ORE, ORE, GLASS, LOOM), GEAR));
		cards.add(new ScientificCard("University", 3, age, Lists.newArrayList(LUMBER, LUMBER, PAPYRUS, GLASS), TABLET));
		cards.add(new ScientificCard("University", 4, age, Lists.newArrayList(LUMBER, LUMBER, PAPYRUS, GLASS), TABLET));
		cards.add(new ScientificCard("Academy", 3, age, Lists.newArrayList(STONE, STONE, STONE, GLASS), COMPASS));
		cards.add(new ScientificCard("Academy", 7, age, Lists.newArrayList(STONE, STONE, STONE, GLASS), COMPASS));
		cards.add(new ScientificCard("Study", 3, age, Lists.newArrayList(LUMBER, PAPYRUS, LOOM), GEAR));
		cards.add(new ScientificCard("Study", 5, age, Lists.newArrayList(LUMBER, PAPYRUS, LOOM), GEAR));
		cards.add(new CommercialCard("Haven", 3, age, Lists.newArrayList(LOOM, ORE, LUMBER), Algorithms.gainGoldAlgorithm(1, Card.Type.MATERIAL, false), Algorithms.addVictoryPointAlgorithm(Algorithms.pointsForMyCards(1, Card.Type.MATERIAL))));
		cards.add(new CommercialCard("Haven", 4, age, Lists.newArrayList(LOOM, ORE, LUMBER), Algorithms.gainGoldAlgorithm(1, Card.Type.MATERIAL, false), Algorithms.addVictoryPointAlgorithm(Algorithms.pointsForMyCards(1, Card.Type.MATERIAL))));
		cards.add(new CommercialCard("Lighthouse", 3, age, Lists.newArrayList(GLASS, STONE), Algorithms.gainGoldAlgorithm(1, Card.Type.COMMERCIAL, false), Algorithms.addVictoryPointAlgorithm(Algorithms.pointsForMyCards(1, Card.Type.COMMERCIAL))));
		cards.add(new CommercialCard("Lighthouse", 6, age, Lists.newArrayList(GLASS, STONE), Algorithms.gainGoldAlgorithm(1, Card.Type.COMMERCIAL, false), Algorithms.addVictoryPointAlgorithm(Algorithms.pointsForMyCards(1, Card.Type.COMMERCIAL))));
		cards.add(new CommercialCard("Chamber of Commerce", 4, age, Lists.newArrayList(CLAY, CLAY, PAPYRUS), Algorithms.gainGoldAlgorithm(2, Card.Type.GOOD, false), Algorithms.addVictoryPointAlgorithm(Algorithms.pointsForMyCards(1, Card.Type.GOOD))));
		cards.add(new CommercialCard("Chamber of Commerce", 6, age, Lists.newArrayList(CLAY, CLAY, PAPYRUS), Algorithms.gainGoldAlgorithm(2, Card.Type.GOOD, false), Algorithms.addVictoryPointAlgorithm(Algorithms.pointsForMyCards(1, Card.Type.GOOD))));
		cards.add(new CommercialCard("Arena", 3, age, Lists.newArrayList(ORE, STONE, STONE), Algorithms.gainGoldFromMyWonderStages(3), Algorithms.addVictoryPointAlgorithm(Algorithms.pointsForWonderStages(1, false))));
		cards.add(new CommercialCard("Arena", 5, age, Lists.newArrayList(ORE, STONE, STONE), Algorithms.gainGoldFromMyWonderStages(3), Algorithms.addVictoryPointAlgorithm(Algorithms.pointsForWonderStages(1, false))));
		cards.add(new CommercialCard("Arena", 7, age, Lists.newArrayList(ORE, STONE, STONE), Algorithms.gainGoldFromMyWonderStages(3), Algorithms.addVictoryPointAlgorithm(Algorithms.pointsForWonderStages(1, false))));
		cards.add(new GuildCard("Workers Guild", 3, age, Lists.newArrayList(ORE, ORE, CLAY, STONE, LUMBER), Algorithms.pointsForNeighborsCards(1, Card.Type.MATERIAL)));
		cards.add(new GuildCard("Craftsmens Guild", 3, age, Lists.newArrayList(ORE, ORE, STONE, STONE), Algorithms.pointsForNeighborsCards(2, Card.Type.GOOD)));
		cards.add(new GuildCard("Traders Guild", 3, age, Lists.newArrayList(LOOM, PAPYRUS, GLASS), Algorithms.pointsForNeighborsCards(1, Card.Type.COMMERCIAL)));
		cards.add(new GuildCard("Philosophers Guild", 3, age, Lists.newArrayList(CLAY, CLAY, CLAY, LOOM, PAPYRUS), Algorithms.pointsForNeighborsCards(1, Card.Type.SCIENTIFIC)));
		cards.add(new GuildCard("Spies Guild", 3, age, Lists.newArrayList(CLAY, CLAY, CLAY, GLASS), Algorithms.pointsForNeighborsCards(1, Card.Type.MILITARY)));
		cards.add(new GuildCard("Strategists Guild", 3, age, Lists.newArrayList(ORE, ORE, STONE, LOOM), Algorithms.pointsForNeighborsMilitaryLosses(1)));
		cards.add(new GuildCard("Shipowners Guild", 3, age, Lists.newArrayList(LUMBER, LUMBER, LUMBER, PAPYRUS, GLASS), Algorithms.pointsForMyCards(1, Card.Type.MATERIAL), Algorithms.pointsForMyCards(1, Card.Type.GOOD), Algorithms.pointsForMyCards(1, Card.Type.GUILD)));
		cards.add(new GuildCard("Magistrates Guild", 3, age, Lists.newArrayList(LUMBER, LUMBER, LUMBER, STONE, LOOM), Algorithms.pointsForNeighborsCards(1, Card.Type.CIVILIAN)));
		cards.add(new GuildCard("Builders Guild", 3, age, Lists.newArrayList(STONE, STONE, CLAY, CLAY, GLASS), Algorithms.pointsForWonderStages(1, true)));
		cards.add(new GuildCard("Scientists Guild", 3, age, Lists.newArrayList(LUMBER, LUMBER, ORE, ORE, PAPYRUS), new VictoryPointCountingAlgorithm[]{})); //Algorithm baked into default endGameScientific algorithm
		
		//Create indexes for quick and easy reads
		cardIndex = Maps.newHashMap();
		goodsIndex = Maps.newHashMap();
		guildCards = Lists.newArrayList();
		for (Card card : cards){
			if (!cardIndex.containsKey(card.getName()))
			{
				cardIndex.put(card.getName(), Maps.<Integer, Card>newHashMap());
			}
			cardIndex.get(card.getName()).put(card.getNumPlayers(), card);
			//The previous cardIndex is not sufficient to distinguish between the Goods cards of ages 1 and 2
			if (card.getType() == Card.Type.GOOD){
				String name = card.getName();
				Integer numPlayers = card.getNumPlayers();
				if (!goodsIndex.containsKey(name))
				{
					goodsIndex.put(name, Maps.<Integer, Map<Integer, Card>>newHashMap());
				}
				if (!goodsIndex.get(name).containsKey(numPlayers))
				{
					goodsIndex.get(name).put(numPlayers, Maps.<Integer, Card>newHashMap());
				}
				goodsIndex.get(name).get(numPlayers).put(card.getAge(), card);
			}
			//Quick access to guild cards to shuffle into age-3 decks
			else if (card.getType() == Card.Type.GUILD){
				guildCards.add(card);
			}
		}
		
		//Wire up card hierarchies
		addDescendantsToEachAndReverse("Baths", "Aqueduct");
		addDescendantsToEachAndReverse("Altar", "Temple");
		addDescendantsToEachAndReverse("Temple", "Pantheon");
		addDescendantsToEachAndReverse("Theater", "Statue");
		addDescendantsToEachAndReverse("Statue", "Gardens");
		addDescendantsToEachAndReverse("Walls", "Fortifications");
		addDescendantsToEachAndReverse("Training Ground", "Circus");
		addDescendantsToEachAndReverse("Apothecary", "Stables", "Dispensary");
		addDescendantsToEachAndReverse("Dispensary", "Arena", "Lodge");
		addDescendantsToEachAndReverse("Workshop", "Archery Range", "Laboratory");
		addDescendantsToEachAndReverse("Laboratory", "Siege Workshop", "Observatory");
		addDescendantsToEachAndReverse("Scriptorium", "Courthouse", "Library");
		addDescendantsToEachAndReverse("Library", "Senate", "University");
		addDescendantsToEachAndReverse("School", "Academy", "Study");
		addDescendantsToEachAndReverse("East Trading Post", "Forum");
		addDescendantsToEachAndReverse("West Trading Post", "Forum");
		addDescendantsToEachAndReverse("Forum", "Haven");
		addDescendantsToEachAndReverse("Marketplace", "Caravansery");
		addDescendantsToEachAndReverse("Caravansery", "Lighthouse");
	}
	
	private CardFactory(){}
	
	public static Card fromMap(Map<String, Object> map) throws IllegalArgumentException
	{
		String name = (String)map.get("name");
		int numPlayers = ((Double)map.get("numPlayers")).intValue();
		int age = ((Double)map.get("age")).intValue();
		return getCard(name, numPlayers, age);
	}
	
	public static Card getCard(String name, int numPlayers, int age){
		if (goodsIndex.containsKey(name))
		{
			return goodsIndex.get(name).get(numPlayers).get(age);
		}
		return cardIndex.get(name).get(numPlayers);
	}
	
	private static List<Card> addInGuildCards(int numPlayers){
		List<Card> guilds = Lists.newArrayList(guildCards);
		Collections.shuffle(guilds);
		while (guilds.size() > numPlayers + 2){
			guilds.remove(guilds.size() - 1);
		}
		return guilds;
	}
	
	public static List<Card> getDeck(int age, int numPlayers){
		List<Card> deck = Lists.newArrayList();
		for (Card card : cards){
			if (card.getAge() == age && card.getType() != Card.Type.GUILD && card.getNumPlayers() <= numPlayers){
				deck.add(card);
			}
		}
		if (age == 3){
			deck.addAll(addInGuildCards(numPlayers));
		}
		Collections.shuffle(deck);
		return deck;
	}
}
