package com.jeffrpowell.sevenwonders.cards;

import com.jeffrpowell.sevenwonders.ResourceType;
import com.jeffrpowell.sevenwonders.game.Game.PrivateGameState;
import com.jeffrpowell.sevenwonders.game.algorithms.VictoryPointCountingAlgorithm;
import java.util.Arrays;
import java.util.List;

public class GuildCard extends Card{
	private static final Card.Type type = Card.Type.GUILD;
	private final List<VictoryPointCountingAlgorithm> algorithms;

	public GuildCard(String name, int numPlayers, int age, List<ResourceType> cost, VictoryPointCountingAlgorithm... algorithms){
		super(name, type, numPlayers, age, cost);
		this.algorithms = Arrays.asList(algorithms);
	}

	public void addVictoryPointAlgorithms(PrivateGameState gameState){
		gameState.endGamePointAlgorithms.addAll(algorithms);
	}
}
