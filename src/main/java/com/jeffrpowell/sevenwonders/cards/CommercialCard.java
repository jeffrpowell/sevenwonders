package com.jeffrpowell.sevenwonders.cards;

import com.jeffrpowell.sevenwonders.ResourceType;
import com.jeffrpowell.sevenwonders.game.Game.PrivateGameState;
import com.jeffrpowell.sevenwonders.game.algorithms.ChangeGameStateAlgorithm;
import java.util.Arrays;
import java.util.List;

public class CommercialCard extends Card{
	private static final Card.Type type = Card.Type.COMMERCIAL;
	private final List<ChangeGameStateAlgorithm> algorithms;

	public CommercialCard(String name, int numPlayers, int age, List<ResourceType> cost, ChangeGameStateAlgorithm... algorithms){
		super(name, type, numPlayers, age, cost);
		this.algorithms = Arrays.asList(algorithms);
	}

	public void alterGameState(PrivateGameState gameState){
		for (ChangeGameStateAlgorithm algorithm : algorithms)
		{
			algorithm.alterGameState(gameState);
		}
	}
}
