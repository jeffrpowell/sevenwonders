package com.jeffrpowell.sevenwonders.cards;

import com.jeffrpowell.sevenwonders.ResourceType;
import java.util.List;

public class MilitaryCard extends Card{
	private static final Card.Type type = Card.Type.MILITARY;
	private final int shields;

	public MilitaryCard(String name, int numPlayers, int age, List<ResourceType> cost, int shields){
		super(name, type, numPlayers, age, cost);
		this.shields = shields;
	}

	public int getShields(){
		return shields;
	}
}
