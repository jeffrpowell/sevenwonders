package com.jeffrpowell.sevenwonders.cards;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.gson.annotations.Expose;
import com.jeffrpowell.sevenwonders.ResourceType;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public class Card {
	public static enum Type{
		MATERIAL, //Brown - Raw Materials
		GOOD, //Gray - Manufactured Goods
		CIVILIAN, //Blue - Victory points
		SCIENTIFIC, //Green - Scientific fields
		COMMERCIAL, //Yellow - Variety
		MILITARY, //Red - Shields
		GUILD //Purple - Variable Victory points
	}
	@Expose private final String name;
	@Expose private final Type type;
	@Expose private final int numPlayers;
	@Expose private final int age;
	private final List<ResourceType> cost;
	private final Set<Card> ancestors;
	private final Set<Card> descendants;

	public Card(String name, Type type, int numPlayers, int age, List<ResourceType> cost){
		this.name = name;
		this.type = type;
		this.numPlayers = numPlayers;
		this.age = age;
		this.cost = cost;
		this.ancestors = Sets.newHashSet();
		this.descendants = Sets.newHashSet();
	}

	public String getName(){
		return name;
	}
	
	public Type getType(){
		return type;
	}

	public int getNumPlayers(){
		return numPlayers;
	}
	
	public int getAge(){
		return age;
	}
	
	public List<ResourceType> getCost(){
		return Lists.newArrayList(cost);
	}

	public Set<Card> getAncestors(){
		return ancestors;
	}
	
	public void addAncestors(Card... ancestors)
	{
		this.ancestors.addAll(Arrays.asList(ancestors));
	}
	
	public void addAncestors(Collection<Card> ancestors)
	{
		this.ancestors.addAll(ancestors);
	}

	public Set<Card> getDescendants(){
		return descendants;
	}
	
	public void addDescendants(Card... descendants)
	{
		this.descendants.addAll(Arrays.asList(descendants));
	}
	
	public void addDescendants(Collection<Card> descendants)
	{
		this.descendants.addAll(descendants);
	}
}
