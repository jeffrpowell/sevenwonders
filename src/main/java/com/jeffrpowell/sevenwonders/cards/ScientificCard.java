package com.jeffrpowell.sevenwonders.cards;

import com.jeffrpowell.sevenwonders.ResourceType;
import java.util.List;

public class ScientificCard extends Card{
	public static enum Symbol {GEAR, COMPASS, TABLET}
	private static final Card.Type type = Card.Type.SCIENTIFIC;
	private final Symbol symbol;

	public ScientificCard(String name, int numPlayers, int age, List<ResourceType> cost, Symbol symbol){
		super(name, type, numPlayers, age, cost);
		this.symbol = symbol;
	}

	public Symbol getSymbol(){
		return symbol;
	}
}
