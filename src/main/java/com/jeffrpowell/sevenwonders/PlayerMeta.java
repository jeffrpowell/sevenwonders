package com.jeffrpowell.sevenwonders;

import com.google.gson.annotations.Expose;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

public class PlayerMeta {
	@Expose private final String username;
	private final UUID sessionId;
	private final int gameId;
	
	public PlayerMeta(String username)
	{
		this.username = username;
		this.sessionId = UUID.randomUUID();
		this.gameId = -1;
	}

	public PlayerMeta(String username, UUID sessionId){
		this.username = username;
		this.sessionId = sessionId;
		this.gameId = -1;
	}

	public PlayerMeta(String username, UUID sessionId, int gameId){
		this.username = username;
		this.sessionId = sessionId;
		this.gameId = gameId;
	}

	public String getUsername(){
		return username;
	}

	public UUID getSessionId(){
		return sessionId;
	}
	
	public int getGameId(){
		return gameId;
	}
	
	public static PlayerMeta fromMap(Map<String, Object> map) throws IllegalArgumentException
	{
		String username = (String)map.get("username");
		if (username == null)
		{
			throw new IllegalArgumentException("PlayerMeta.fromMap() requires a username key.");
		}
		username = JsonUtils.urlEncode(username);
		if (map.containsKey("gameId"))
		{
			int gameId = Integer.parseInt((String)map.get("gameId"));
			UUID sessionId = UUID.fromString((String)map.get("sessionId"));
			return new PlayerMeta(username, sessionId, gameId);
		}
		else if (map.containsKey("sessionId"))
		{
			UUID sessionId = UUID.fromString((String)map.get("sessionId"));
			return new PlayerMeta(username, sessionId);
		}
		else
		{
			return new PlayerMeta(username);
		}
	}
	
	@Override
	public String toString(){
		return username;
	}

	@Override
	public int hashCode(){
		int hash = 5;
		hash = 29 * hash + Objects.hashCode(this.username);
		hash = 29 * hash + Objects.hashCode(this.sessionId);
		return hash;
	}

	@Override
	public boolean equals(Object obj){
		if (obj == null){
			return false;
		}
		if (getClass() != obj.getClass()){
			return false;
		}
		final PlayerMeta other = (PlayerMeta) obj;
		if (!Objects.equals(this.username, other.username)){
			return false;
		}
		return Objects.equals(this.sessionId, other.sessionId);
	}
}
