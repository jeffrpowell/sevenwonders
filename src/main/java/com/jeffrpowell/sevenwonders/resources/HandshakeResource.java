package com.jeffrpowell.sevenwonders.resources;

import com.jeffrpowell.sevenwonders.HandshakeMessage;
import com.jeffrpowell.sevenwonders.JsonUtils;
import com.jeffrpowell.sevenwonders.PlayerMeta;
import com.jeffrpowell.sevenwonders.sockets.GameEndpoint;
import com.jeffrpowell.sevenwonders.sockets.LobbyEndpoint;
import java.util.Set;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/session")
public class HandshakeResource {
	@GET
	@Path("{username}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getNewSession(@PathParam("username") String username)
	{
		if (username != null && !username.isEmpty())
		{
			Set<PlayerMeta> currentPlayers = LobbyEndpoint.getConnectedPlayers();
			currentPlayers.addAll(GameEndpoint.getConnectedPlayers());
			for (PlayerMeta player : currentPlayers){
				if (player.getUsername().equals(username))
				{
					return Response.status(Response.Status.NO_CONTENT).entity("Username is already taken by another player.").build();
				}
			}
			return Response.status(Response.Status.CREATED).entity(JsonUtils.objectToJson(new HandshakeMessage(new PlayerMeta(username)))).build();
		}
		return Response.status(Response.Status.BAD_REQUEST).entity("Must provide a non-empty username in the path.").build();
	}
}
