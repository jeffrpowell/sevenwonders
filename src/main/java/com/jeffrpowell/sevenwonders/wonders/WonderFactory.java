package com.jeffrpowell.sevenwonders.wonders;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import static com.jeffrpowell.sevenwonders.ResourceType.CLAY;
import static com.jeffrpowell.sevenwonders.ResourceType.GLASS;
import static com.jeffrpowell.sevenwonders.ResourceType.LOOM;
import static com.jeffrpowell.sevenwonders.ResourceType.LUMBER;
import static com.jeffrpowell.sevenwonders.ResourceType.ORE;
import static com.jeffrpowell.sevenwonders.ResourceType.PAPYRUS;
import static com.jeffrpowell.sevenwonders.ResourceType.STONE;
import com.jeffrpowell.sevenwonders.game.algorithms.Algorithms;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class WonderFactory {
	private WonderFactory(){}
	
	private static final Set<Wonder> wonders;
	private static final Map<Boolean, Map<String, Wonder>> wonderIndex;
	
	static{
		wonders = Sets.newHashSet();
		boolean side = true; //Side A
		wonders.add(new Wonder("The Colossus of Rhódos", "Rhodos", side, ORE,
			new WonderStage(Lists.newArrayList(LUMBER, LUMBER), 3, Algorithms.noOpChangeAlgorithm()),
			new WonderStage(Lists.newArrayList(CLAY, CLAY, CLAY), 0, Algorithms.addRhodosShieldsAlgorithm(2)),
			new WonderStage(Lists.newArrayList(ORE, ORE, ORE, ORE), 7, Algorithms.noOpChangeAlgorithm())));
		wonders.add(new Wonder("The Lighthouse of Alexandria", "Alexandria", side, GLASS,
			new WonderStage(Lists.newArrayList(STONE, STONE), 3, Algorithms.noOpChangeAlgorithm()),
			new WonderStage(Lists.newArrayList(ORE, ORE), 0, Algorithms.enableAlexandriaMaterials()),
			new WonderStage(Lists.newArrayList(GLASS, GLASS), 7, Algorithms.noOpChangeAlgorithm())));
		wonders.add(new Wonder("The Temple of Artemis at Éphesos", "Ephesos", side, PAPYRUS,
			new WonderStage(Lists.newArrayList(STONE, STONE), 3, Algorithms.noOpChangeAlgorithm()),
			new WonderStage(Lists.newArrayList(LUMBER, LUMBER), 0, Algorithms.gainGoldAlgorithm(9, null, false)),
			new WonderStage(Lists.newArrayList(PAPYRUS, PAPYRUS), 7, Algorithms.noOpChangeAlgorithm())));
		wonders.add(new Wonder("The Hanging Gardens of Babylon", "Babylon", side, CLAY,
			new WonderStage(Lists.newArrayList(CLAY, CLAY), 3, Algorithms.noOpChangeAlgorithm()),
			new WonderStage(Lists.newArrayList(LUMBER, LUMBER, LUMBER), 0, Algorithms.enableBabylonScienceSymbol()),
			new WonderStage(Lists.newArrayList(CLAY, CLAY, CLAY, CLAY), 7, Algorithms.noOpChangeAlgorithm())));
		wonders.add(new Wonder("The Temple of Zeus in Olympía", "Olympia", side, LUMBER,
			new WonderStage(Lists.newArrayList(LUMBER, LUMBER), 3, Algorithms.noOpChangeAlgorithm()),
			new WonderStage(Lists.newArrayList(STONE, STONE), 0, Algorithms.enableOlympiaFreebies()),
			new WonderStage(Lists.newArrayList(ORE, ORE), 7, Algorithms.noOpChangeAlgorithm())));
		wonders.add(new Wonder("The Mausoleum of Halikarnassós", "Halikarnassos", side, LOOM,
			new WonderStage(Lists.newArrayList(CLAY, CLAY), 3, Algorithms.noOpChangeAlgorithm()),
			new WonderStage(Lists.newArrayList(ORE, ORE, ORE), 0, Algorithms.enableHalikarnassosDiscardSearch()),
			new WonderStage(Lists.newArrayList(LOOM, LOOM), 7, Algorithms.noOpChangeAlgorithm())));
		wonders.add(new Wonder("The Pyramids of Gizah", "Gizah", side, STONE,
			new WonderStage(Lists.newArrayList(STONE, STONE), 3, Algorithms.noOpChangeAlgorithm()),
			new WonderStage(Lists.newArrayList(LUMBER, LUMBER, LUMBER), 5, Algorithms.noOpChangeAlgorithm()),
			new WonderStage(Lists.newArrayList(STONE, STONE, STONE, STONE), 7, Algorithms.noOpChangeAlgorithm())));
		side = false; //Side B
		wonders.add(new Wonder("The Colossus of Rhódos", "Rhodos", side, ORE,
			new WonderStage(Lists.newArrayList(STONE, STONE, STONE), 3, Algorithms.addRhodosShieldsAlgorithm(1), Algorithms.gainGoldAlgorithm(3, null, false)),
			new WonderStage(Lists.newArrayList(ORE, ORE, ORE, ORE), 4, Algorithms.addRhodosShieldsAlgorithm(1), Algorithms.gainGoldAlgorithm(4, null, false))));
		wonders.add(new Wonder("The Lighthouse of Alexandria", "Alexandria", side, GLASS,
			new WonderStage(Lists.newArrayList(CLAY, CLAY), 0, Algorithms.enableAlexandriaMaterials()),
			new WonderStage(Lists.newArrayList(LUMBER, LUMBER), 0, Algorithms.enableAlexandriaGoods()),
			new WonderStage(Lists.newArrayList(STONE, STONE, STONE), 7, Algorithms.noOpChangeAlgorithm())));
		wonders.add(new Wonder("The Temple of Artemis at Éphesos", "Ephesos", side, PAPYRUS,
			new WonderStage(Lists.newArrayList(STONE, STONE), 2, Algorithms.gainGoldAlgorithm(4, null, false)),
			new WonderStage(Lists.newArrayList(LUMBER, LUMBER), 3, Algorithms.gainGoldAlgorithm(4, null, false)),
			new WonderStage(Lists.newArrayList(GLASS, LOOM, PAPYRUS), 5, Algorithms.gainGoldAlgorithm(4, null, false))));
		wonders.add(new Wonder("The Hanging Gardens of Babylon", "Babylon", side, CLAY,
			new WonderStage(Lists.newArrayList(CLAY, LOOM), 3, Algorithms.noOpChangeAlgorithm()),
			new WonderStage(Lists.newArrayList(LUMBER, LUMBER, GLASS), 0, Algorithms.enableBabylonSeventhPlay()),
			new WonderStage(Lists.newArrayList(CLAY, CLAY, CLAY, PAPYRUS), 0, Algorithms.enableBabylonScienceSymbol())));
		wonders.add(new Wonder("The Temple of Zeus in Olympía", "Olympia", side, LUMBER,
			new WonderStage(Lists.newArrayList(LUMBER, LUMBER), 0, Algorithms.cheaperResourcesAlgorithm(true, true), Algorithms.cheaperResourcesAlgorithm(true, false)),
			new WonderStage(Lists.newArrayList(STONE, STONE), 5, Algorithms.enableOlympiaFreebies()),
			new WonderStage(Lists.newArrayList(ORE, ORE, LOOM), 0, Algorithms.enableOlympiaCloneGuild())));
		wonders.add(new Wonder("The Mausoleum of Halikarnassós", "Halikarnassos", side, LOOM,
			new WonderStage(Lists.newArrayList(ORE, ORE), 2, Algorithms.enableHalikarnassosDiscardSearch()),
			new WonderStage(Lists.newArrayList(CLAY, CLAY, CLAY), 1, Algorithms.enableHalikarnassosDiscardSearch()),
			new WonderStage(Lists.newArrayList(LOOM, PAPYRUS, GLASS), 0, Algorithms.enableHalikarnassosDiscardSearch())));
		wonders.add(new Wonder("The Pyramids of Gizah", "Gizah", side, STONE,
			new WonderStage(Lists.newArrayList(LUMBER, LUMBER), 3, Algorithms.noOpChangeAlgorithm()),
			new WonderStage(Lists.newArrayList(STONE, STONE, STONE), 5, Algorithms.noOpChangeAlgorithm()),
			new WonderStage(Lists.newArrayList(CLAY, CLAY, CLAY), 5, Algorithms.noOpChangeAlgorithm()),
			new WonderStage(Lists.newArrayList(STONE, STONE, STONE, STONE, PAPYRUS), 7, Algorithms.noOpChangeAlgorithm())));
		
		//Create index for easy reads
		wonderIndex = Maps.newHashMap();
		wonderIndex.put(true, Maps.<String, Wonder>newHashMap());
		wonderIndex.put(false, Maps.<String, Wonder>newHashMap());
		for (Wonder wonder : wonders){
			wonderIndex.get(wonder.isSideA()).put(wonder.getShortName(), wonder);
		}
	}
	
	public static Wonder getWonder(boolean sideA, String shortName){
		return wonderIndex.get(sideA).get(shortName);
	}
	
	public static List<Wonder> getRandomWonders(int numPlayers){
		List<Wonder> wonderCopies = Lists.newArrayList();
		for (Wonder wonder : wonderIndex.get(true).values()){
			wonderCopies.add(new Wonder(wonder));
		}
		Collections.shuffle(wonderCopies);
		return wonderCopies.subList(0, numPlayers);
	}
}
