package com.jeffrpowell.sevenwonders.wonders;

import com.google.common.collect.Lists;
import com.google.gson.annotations.Expose;
import com.jeffrpowell.sevenwonders.ResourceType;
import com.jeffrpowell.sevenwonders.cards.Card;
import com.jeffrpowell.sevenwonders.game.Game;
import java.util.Arrays;
import java.util.List;

public class Wonder {
	private final String fullName;
	@Expose private final String shortName;
	@Expose private final boolean sideA;
	@Expose private final ResourceType resource;
	@Expose private final List<WonderStage> stages;

	public Wonder(String fullName, String shortName, boolean sideA, ResourceType resource, WonderStage... stages){
		this.fullName = fullName;
		this.shortName = shortName;
		this.sideA = sideA;
		this.resource = resource;
		this.stages = Arrays.asList(stages);
	}
	
	public Wonder(Wonder other){
		this.fullName = other.fullName;
		this.shortName = other.shortName;
		this.sideA = other.sideA;
		this.resource = other.resource;
		this.stages = Lists.newArrayList();
		for (WonderStage stage : other.stages){
			this.stages.add(new WonderStage(stage));
		}
	}
	
	public void addStage(WonderStage stage){
		stages.add(stage);
	}

	public String getFullName(){
		return fullName;
	}

	public String getShortName(){
		return shortName;
	}

	public boolean isSideA(){
		return sideA;
	}

	public ResourceType getResource(){
		return resource;
	}

	public List<WonderStage> getStages(){
		return stages;
	}
	
	public int numBuiltStages(){
		int total = 0;
		for (WonderStage stage : stages){
			total += stage.isBuilt() ? 1 : 0;
		}
		return total;
	}
	
	public int getEarnedVictoryPoints(){
		int total = 0;
		for (WonderStage stage : stages){
			if (stage.isBuilt())
			{
				total += stage.getVictoryPoints();
			}
		}
		return total;
	}
	
	public WonderStage getNextStage(){
		for (WonderStage stage : stages){
			if (!stage.isBuilt())
			{
				return stage;
			}
		}
		return null;
	}
	
	public void buildNextStage(Card burntCard, Game.PrivateGameState gameState){
		WonderStage stage = getNextStage();
		if (stage != null){
			stage.build(burntCard);
			stage.applyAlgorithms(gameState);
		}
	}
}
