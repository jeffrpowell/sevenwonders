package com.jeffrpowell.sevenwonders.wonders;

import com.google.common.collect.Lists;
import com.google.gson.annotations.Expose;
import com.jeffrpowell.sevenwonders.ResourceType;
import com.jeffrpowell.sevenwonders.cards.Card;
import com.jeffrpowell.sevenwonders.game.Game;
import com.jeffrpowell.sevenwonders.game.algorithms.ChangeGameStateAlgorithm;
import java.util.Arrays;
import java.util.List;

public class WonderStage {
	@Expose private final List<ResourceType> cost;
	@Expose private final int victoryPoints;
	private final List<ChangeGameStateAlgorithm> algorithms;
	@Expose private Card spentCard;

	public WonderStage(List<ResourceType> cost, int victoryPoints, ChangeGameStateAlgorithm... algorithms){
		this.cost = cost;
		this.victoryPoints = victoryPoints;
		this.algorithms = Arrays.asList(algorithms);
		this.spentCard = null;
	}
	
	public WonderStage(WonderStage other){
		this.cost = other.cost;
		this.victoryPoints = other.victoryPoints;
		this.algorithms = other.algorithms;
		this.spentCard = null;
	}
	
	public boolean isBuilt()
	{
		return spentCard != null;
	}
	
	public void build(Card card)
	{
		this.spentCard = card;
	}

	public List<ResourceType> getCost(){
		return Lists.newArrayList(cost);
	}

	public int getVictoryPoints(){
		return victoryPoints;
	}

	public void applyAlgorithms(Game.PrivateGameState gameState){
		for (ChangeGameStateAlgorithm algorithm : algorithms){
			algorithm.alterGameState(gameState);
		}
	}
}
