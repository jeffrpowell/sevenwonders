package com.jeffrpowell.sevenwonders;

import java.util.Map;

public class DefaultMessage extends Message{

	public DefaultMessage(ID id, PlayerMeta player)
	{
		super(id, player);
	}
	
	public static DefaultMessage fromMap(Map<String, Object> map)
	{
		ID id = ID.valueOf((String)map.get("id"));
		PlayerMeta player = PlayerMeta.fromMap((Map<String, Object>)map.get("player"));
		return new DefaultMessage(id, player);
	}
}
