package com.jeffrpowell.sevenwonders;

import com.google.gson.annotations.Expose;
import java.util.Map;

public abstract class Message {
	public static enum ID{
		ERROR,				// General use for error messages
		HANDSHAKE,			// Establishing connections with the sockets
		
		/*LOBBY MESSAGES*/
		
		LOBBY_STATE,		// Carries information about games available in lobby
		NEW_GAME_STATE,		// Carries information about a specific game you've joined
		NEW_GAME,			// Creates a new game in the lobby
		JOIN_GAME,			// Declare intent to join a game
		LEAVE_GAME,			// Declare intent to leave a previously joined game
		RANDOM_WONDERS,		// Change how you want to divi out wonders
		SELECT_WONDER,		// Choose your wonder board
		NEW_CHAT_MSG,		// Send a chat message to everyone else in the same game
		START_GAME,			// Attempts to start a new game
		
		/*GAME MESSAGES*/
		
		GAME_STATE,			// Carries information about your hand and everyone's boards
		PURCHASE_RESOURCE,	// Buy a resource from your neighbor
		CARD_PLAY_OPTIONS,	// Lets you know what you can do with a previously selected card
		PLAY_CARD,			// Sends one of the card_play_options back; You're either selecting a card or playing it
		BABYLON_PLAY,		// Shows intent of Babylon player to use special ability to play both cards 6 and 7
		SEARCH_DISCARD,		// Carries the discard pile for Halikarnassos' special wonder ability
		SCORE,				// Gives the current snapshot of scores for all players
		PICK_UP_HAND;		// Tells the server that you're ready to pick up your next hand (get a new game state)
		
		public static ID fromMap(Map<String, Object> map)
		{
			return ID.valueOf((String)map.get("id"));
		}
	}
	@Expose protected final PlayerMeta player;
	@Expose private final ID id;
	public Message(ID id, PlayerMeta player)
	{
		this.id = id;
		this.player = player;
	}
	public ID getId()
	{
		return id;
	}
	
	public PlayerMeta getPlayer()
	{
		return player;
	}
}
