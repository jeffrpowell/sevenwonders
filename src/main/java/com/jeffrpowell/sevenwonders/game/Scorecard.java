package com.jeffrpowell.sevenwonders.game;

import com.google.common.collect.Maps;
import com.google.gson.annotations.Expose;
import com.jeffrpowell.sevenwonders.PlayerMeta;
import java.util.Map;

public class Scorecard {
	@Expose private final Map<PlayerMeta, Integer> scores;
	@Expose private final int age;
	@Expose private final boolean finalScore;
	
	public Scorecard(int age, boolean finalScore)
	{
		scores = Maps.newHashMap();
		this.age = age;
		this.finalScore = finalScore;
	}
	
	public void put(PlayerMeta player, Integer score){
		scores.put(player, score);
	}
}
