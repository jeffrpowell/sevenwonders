package com.jeffrpowell.sevenwonders.game;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.jeffrpowell.sevenwonders.DefaultMessage;
import com.jeffrpowell.sevenwonders.Message;
import com.jeffrpowell.sevenwonders.PlayerMeta;
import com.jeffrpowell.sevenwonders.SingleStringMessage;
import com.jeffrpowell.sevenwonders.cards.Card;
import com.jeffrpowell.sevenwonders.game.Game.CardPlayOptions;
import com.jeffrpowell.sevenwonders.game.Game.PrivateGameState;
import com.jeffrpowell.sevenwonders.game.messages.PlayCardMessage;
import com.jeffrpowell.sevenwonders.game.messages.PurchaseResourceMessage;
import com.jeffrpowell.sevenwonders.game.messages.PurchaseWonderResourceMessage;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public class GameController {
	private final Random random;
	private final Map<Integer, Game> games;
	private final Set<GameListener> listeners;
	
	public GameController()
	{
		this.random = new Random();
		this.games = Maps.newHashMap();
		this.listeners = Sets.newHashSet();
	}
	
	public void addListener(GameListener listener)
	{
		listeners.add(listener);
	}
	
	public int startNewGame(GameSetup options)
	{
		int gameId = random.nextInt(1000000000);
		while (games.containsKey(gameId))
		{
			//just in case of collision
			gameId = random.nextInt(1000000000);
		}
		games.put(gameId, new Game(options));
		return gameId;
	}
	
	public void handleMessage(Map<String, Object> message)
	{
		Message meta = DefaultMessage.fromMap(message);
		if (isValidPlayerAndSession(meta.getPlayer()))
		{
			try
			{
				switch(meta.getId()){
					case HANDSHAKE:
						connectPlayer(message);
						break;
					case NEW_CHAT_MSG:
						addChatMessage(message);
						break;
					case PURCHASE_RESOURCE:
						handleNeighborResourcePurchase(message);
						break;
					case PLAY_CARD:
						playCard(message);
						break;
					case PICK_UP_HAND:
						gameStateChanged(games.get(meta.getPlayer().getGameId()), meta.getPlayer());
						break;
				}
			}
			catch (IllegalArgumentException e)
			{
				sendErrorMessage(meta.getPlayer(), "Error while parsing message input: " + e.getMessage());
			}
		}
	}
	
	/**
	 * To aid in exception debugging
	 * @param player
	 * @return 
	 */
	public PrivateGameState getPrivateGameState(PlayerMeta player)
	{
		if (isValidPlayerAndSession(player))
		{
			return games.get(player.getGameId()).getStateForPlayer(player);
		}
		return null;
	}
	
	private boolean isValidPlayerAndSession(PlayerMeta player)
	{
		int gameId = player.getGameId();
		if (gameId < 0 || !games.containsKey(gameId))
		{
			sendErrorMessage(player, "Invalid game id supplied in handshake message.");
			return false;
		}
		Game game = games.get(gameId);
		if (!game.containsPlayer(player))
		{
			sendErrorMessage(player, "You're asking to join a game you don't belong to.");
			return false;
		}
		return true;
	}
	
	private void connectPlayer(Map<String, Object> message)
	{
		Message msg = DefaultMessage.fromMap(message);
		PlayerMeta player = msg.getPlayer();
		Game game = games.get(player.getGameId());
		boolean previouslyDisconnected = game.isPlayerDisconnected(player);
		game.connectPlayer(player);
		if (game.allPlayersConnected() && !previouslyDisconnected)
		{
			game.assignWonders();
			game.dealOutCards();
			gameStateChanged(game);
		}
		else if (previouslyDisconnected)
		{
			gameStateChanged(game, player);
			chatMessage(game);
		}
	}
	
	private void addChatMessage(Map<String, Object> messageMap){
		SingleStringMessage message = SingleStringMessage.fromMap(messageMap);
		PlayerMeta player = message.getPlayer();
		Game game = games.get(player.getGameId());
		game.addChatLine(player.getUsername(), message.getMsg());
		chatMessage(game);
	}
	
	private void handleNeighborResourcePurchase(Map<String, Object> message)
	{
		try
		{
			PurchaseResourceMessage msg = PurchaseResourceMessage.fromMap(message);
			games.get(msg.getPlayer().getGameId()).handleNeighborResourcePurchase(msg.getPlayer(), msg.getResource(), msg.getSeller());
		}
		catch (IllegalArgumentException | ClassCastException e)
		{
			PurchaseWonderResourceMessage msg = PurchaseWonderResourceMessage.fromMap(message);
			games.get(msg.getPlayer().getGameId()).handleNeighborWonderResourcePurchase(msg.getPlayer(), msg.getResource(), msg.getSeller());
		}
	}
	
	private void selectCard(Map<String, Object> message)
	{
		PlayCardMessage msg = PlayCardMessage.fromMap(message);
		PlayerMeta player = msg.getPlayer();
		giveCardOptions(player, games.get(player.getGameId()).whatCanBeDoneWithCard(player, msg.getCard()));
	}
	
	private void playCard(Map<String, Object> message)
	{
		PlayCardMessage msg = PlayCardMessage.fromMap(message);
		PlayerMeta player = msg.getPlayer();
		Game game = games.get(player.getGameId());
		if (msg.getPlay() == CardPlayOptions.SELECT)
		{
			selectCard(message);
			return;
		}
		if (game.whatCanBeDoneWithCard(player, msg.getCard()).contains(msg.getPlay()))
		{
			game.submitPlay(player, msg.getCard(), msg.getPlay());
			if (game.allPlayersHaveSubmittedPlays()){
				game.executeLastCardActions();
				if (game.allowBabylon7thChoice()){
					sendBabylon7th(game.whoCanUseBabylon7th(), game.getBabylon7thCard(), game.whatCanBeDoneWithCard(player, game.getBabylon7thCard()));
				}
				else if (game.playerBuiltHalikarnassosDiscardSearchStage()){
					sendDiscardPile(game.whoGetsToSearchDiscardPile(), game.getDiscard());
				}
				else{
					game.setupNextTurn();
					sendEndPlayResults(game);
				}
			}
			else
			{
				gameStateChanged(game, player);
			}
		}
		else
		{
			sendErrorMessage(player, "You illegally attempted to "+msg.getPlay()+ " the " + msg.getCard().getName() + " card.");
		}
	}
	
	public void removePlayer(PlayerMeta missingPlayer)
	{
		if (isValidPlayerAndSession(missingPlayer)){
			Game game = games.get(missingPlayer.getGameId());
			for (PlayerMeta player : game.getPlayers()){
				if (player.equals(missingPlayer))
				{
					game.disconnectPlayer(player);
				}
				else if (!game.isEndOfGame())
				{
					sendErrorMessage(player, missingPlayer.getUsername() + " has disconnected from the game. If you stay and they navigate to /7wonders/game.html, the game will resume.");
				}
			}
		}
	}
	
	private void sendErrorMessage(PlayerMeta player, String error){
		for (GameListener listener : listeners){
			listener.error(player, error);
		}
	}
	
	private void gameStateChanged(Game game)
	{
		for (PlayerMeta player : game.getPlayers()){
			gameStateChanged(game, player);
		}
	}
	
	private void gameStateChanged(Game game, PlayerMeta player)
	{
		Game.PrivateGameState gameState = game.getStateForPlayer(player);
		for (GameListener listener : listeners){
			listener.gameStateChanged(player, gameState);
		}
	}
	
	private void giveCardOptions(PlayerMeta player, Set<CardPlayOptions> options)
	{
		for (GameListener listener : listeners){
			listener.giveCardOptions(player, options);
		}
	}
	
	private void sendBabylon7th(PlayerMeta player, Card card, Set<CardPlayOptions> options)
	{
		for (GameListener listener : listeners){
			listener.sendBabylon7th(player, card, options);
		}
	}
	
	private void sendDiscardPile(PlayerMeta player, List<Card> discard){
		for (GameListener listener : listeners){
			listener.sendDiscardPile(player, discard);
		}
	}
	
	private void sendEndPlayResults(Game game)
	{
		Scorecard scorecard = game.getScores();
		for (PlayerMeta player : game.getPlayers()){
			for (GameListener listener : listeners){
				listener.sendEndPlayResults(player, scorecard);
			}
		}
	}
	
	private void chatMessage(Game game)
	{
		List<String> chatLog = game.getChatLog();
		for (PlayerMeta player : game.getPlayers()){
			for (GameListener listener : listeners){
				listener.chatMessage(player, chatLog);
			}
		}
	}
}
