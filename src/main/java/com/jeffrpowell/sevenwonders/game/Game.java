package com.jeffrpowell.sevenwonders.game;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.gson.annotations.Expose;
import com.jeffrpowell.sevenwonders.PlayerMeta;
import com.jeffrpowell.sevenwonders.ResourceType;
import com.jeffrpowell.sevenwonders.cards.Card;
import com.jeffrpowell.sevenwonders.cards.CardFactory;
import com.jeffrpowell.sevenwonders.cards.CommercialCard;
import com.jeffrpowell.sevenwonders.cards.GuildCard;
import com.jeffrpowell.sevenwonders.cards.MilitaryCard;
import com.jeffrpowell.sevenwonders.cards.ResourceCard;
import com.jeffrpowell.sevenwonders.game.algorithms.Algorithms;
import com.jeffrpowell.sevenwonders.game.algorithms.MaxFlow;
import com.jeffrpowell.sevenwonders.game.algorithms.VictoryPointCountingAlgorithm;
import com.jeffrpowell.sevenwonders.wonders.Wonder;
import com.jeffrpowell.sevenwonders.wonders.WonderFactory;
import com.jeffrpowell.sevenwonders.wonders.WonderStage;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Game {
	public static enum CardPlayOptions{
		BUILD, //Play card normally
		WONDER, //Use card to construct section of wonder
		DISCARD, //Discard for coins
		SELECT, //Selecting the card; Allows us to reuse PlayCardMessage when client selects a card
		NOTHING //Used primarily with Babylon's wonder special where the player explicitly decides to do nothing with the 7th card
	} 
	private final GameSetup setup;
	private final Map<PlayerMeta, PrivateGameState> privateStates;
	private final Map<PlayerMeta, PublicGameState> publicStates;
	private final List<Card> discard;
	private final Map<PlayerMeta, CardPlayOptions> submittedPlays;
	private final List<PlayerMeta> disconnectedPlayers;
	private PlayerMeta halikarnassosDiscardPlayer;
	private PlayerMeta babylon7thPlayer;
	private int turnsPlayed;
	private int age;
	private boolean endOfGame;
	private final List<String> chatLog;
	
	public Game(GameSetup setup){
		this.setup = setup;
		this.publicStates = Maps.newHashMap();
		submittedPlays = Maps.newHashMap();
		List<PlayerMeta> players = setup.getPlayers();
		for (int i = 0; i < players.size(); i++){
			PlayerMeta me = players.get(i);
			PlayerMeta leftNeighbor = i == 0 ? players.get(players.size() - 1) : players.get(i - 1);
			PlayerMeta rightNeighbor = i == players.size() - 1 ? players.get(0) : players.get(i + 1);
			publicStates.put(me, new PublicGameState(me, leftNeighbor, rightNeighbor));
			submittedPlays.put(me, null);
		}
		this.privateStates = Maps.newHashMap();
		for (PlayerMeta player : players){
			privateStates.put(player, new PrivateGameState(player, setup.getPlayers(), publicStates));
		}
		this.disconnectedPlayers = Lists.newArrayList();
		discard = Lists.newArrayList();
		halikarnassosDiscardPlayer = null;
		babylon7thPlayer = null;
		age = 0;
		turnsPlayed = 0;
		endOfGame = false;
		chatLog = Lists.newArrayList();
	}
	
	public void assignWonders(){
		if (setup.isRandomWonders()){
			List<Wonder> wonders = WonderFactory.getRandomWonders(getPlayers().size());
			int i = 0;
			for (PublicGameState publicState : publicStates.values()){
				publicState.wonder = wonders.get(i++);
			}
		}
		else{
			for (PublicGameState publicState : publicStates.values()){
				String wonderChoice = setup.getWonderChoices().get(publicState.me);
				publicState.wonder = WonderFactory.getWonder(true, wonderChoice);
			}
		}
	}
	
	public void dealOutCards(){
		age++;
		int numPlayers = getPlayers().size();
		List<Card> deck = CardFactory.getDeck(age, numPlayers);
		List<List<Card>> hands = Lists.partition(deck, deck.size() / numPlayers); // should be size of 7
		int i = 0;
		for (Map.Entry<PlayerMeta, PrivateGameState> privateState : privateStates.entrySet()){
			privateState.getValue().giveInitialHand(hands.get(i++));
		}
	}
	
	public void setupNextTurn(){
		turnsPlayed++;
		if (turnsPlayed == 6){
			turnsPlayed = 0;
			for (PrivateGameState privateGameState : privateStates.values()){
				if (!privateGameState.hand.isEmpty()){
					discard.add(privateGameState.hand.get(0));
					privateGameState.hand.clear();
				}
			}
			executeConflictPhase();
			if (age < 3){
				dealOutCards();
			}
			else{
				endOfGame = true;
			}
		}
		else{
			rotateHands();
		}
	}
	
	private void executeConflictPhase()
	{
		List<PlayerMeta> players = getPlayers();
		for(int iPlayer = 0; iPlayer < publicStates.size(); iPlayer++)
		{
			PrivateGameState privateState = privateStates.get(players.get(iPlayer));
			PublicGameState playerState = privateState.getPublicState();
			PublicGameState leftState = privateState.getLeftNeighborState();
			int playerShields = playerState.getNumShields();
			int leftShields = leftState.getNumShields();
			int victoryAmount = (2 * age) - 1;
			
			if (playerShields > leftShields){
				playerState.conflictTokens.add(victoryAmount);
				leftState.conflictTokens.add(-1);
			}
			else if (leftShields > playerShields){
				leftState.conflictTokens.add(victoryAmount);
				playerState.conflictTokens.add(-1);
			}
		}
	}
	
	private void rotateHands(){
		Map<PlayerMeta, List<Card>> hands = Maps.newHashMap();
		for (PrivateGameState privateState : privateStates.values()){
			List<Card> handToPass = privateState.hand;
			if (age == 1 || age == 3){
				hands.put(privateState.getLeftNeighbor(), handToPass);
			}
			else if (age == 2){
				hands.put(privateState.getRightNeighbor(), handToPass);
			}
		}
		for (Map.Entry<PlayerMeta, List<Card>> passedHands : hands.entrySet()){
			privateStates.get(passedHands.getKey()).hand = passedHands.getValue();
		}
	}
	
	public boolean containsPlayer(PlayerMeta player)
	{
		return setup.getPlayers().contains(player);
	}
	
	public void connectPlayer(PlayerMeta player)
	{
		setup.setPlayerConnected(player, true);
		disconnectedPlayers.remove(player);
	}
	
	public void disconnectPlayer(PlayerMeta player){
		setup.setPlayerConnected(player, false);
		disconnectedPlayers.add(player);
	}
	
	public boolean isPlayerDisconnected(PlayerMeta player){
		return disconnectedPlayers.contains(player);
	}
	
	public boolean allPlayersConnected()
	{
		return setup.allPlayersConnected();
	}
	
	public List<PlayerMeta> getPlayers()
	{
		return setup.getPlayers();
	}
	
	public PrivateGameState getStateForPlayer(PlayerMeta player)
	{
		return privateStates.get(player);
	}
	
	public void handleNeighborResourcePurchase(PlayerMeta buyer, ResourceCard resource, String seller)
	{
		for (PlayerMeta player : getPlayers()){
			if (player.getUsername().equals(seller))
			{
				privateStates.get(buyer).purchaseResource(player, resource);
				break;
			}
		}
	}
	
	public void handleNeighborWonderResourcePurchase(PlayerMeta buyer, ResourceType resource, String seller)
	{
		for (PlayerMeta player : getPlayers()){
			if (player.getUsername().equals(seller))
			{
				privateStates.get(buyer).purchaseWonderResource(player, resource);
				break;
			}
		}
	}
	
	public Set<CardPlayOptions> whatCanBeDoneWithCard(PlayerMeta player, Card card)
	{
		if (submittedPlays.get(player) != null)
		{
			return EnumSet.noneOf(CardPlayOptions.class);
		}
		Set<CardPlayOptions> options = EnumSet.of(CardPlayOptions.DISCARD);
		//if player hasn't played card before and has necessary resources
		PublicGameState gameState = publicStates.get(player);
		if (gameState.hasntBuiltCardYet(card) && gameState.hasResourcesToBuildCard(card)){
			options.add(CardPlayOptions.BUILD);
		}
		//if player has necessary resources
		if (gameState.hasResourcesToBuildNextWonderStage())
		{
			options.add(CardPlayOptions.WONDER);
		}
		return options;
	}
	
	public void executeLastCardActions(){
		for (PrivateGameState state : privateStates.values()){
			state.executeLastCardAction(submittedPlays.get(state.me));
			submittedPlays.put(state.me, null);
		}
	}
	
	public void submitPlay(PlayerMeta player, Card playedCard, CardPlayOptions play){
		if (play == CardPlayOptions.DISCARD || play == CardPlayOptions.NOTHING){
			discard.add(playedCard);
		}
		submittedPlays.put(player, play);
		privateStates.get(player).hand.remove(playedCard);
		privateStates.get(player).lastCardPlayed = playedCard;
		for (List<ResourceCard> purchasedResources : publicStates.get(player).purchasedNeighborResources.values()){
			purchasedResources.clear();
		}
		publicStates.get(player).purchasedNeighborWonderResources.clear();
	}
	
	public boolean allPlayersHaveSubmittedPlays(){
		return !submittedPlays.containsValue(null);
	}
	
	public Scorecard getScores(){
		Scorecard scorecard = new Scorecard(age, endOfGame);
		for (Map.Entry<PlayerMeta, PrivateGameState> privateState : privateStates.entrySet()){
			scorecard.put(privateState.getKey(), privateState.getValue().countTotalScore());
		}
		return scorecard;
	}
	
	public boolean isEndOfGame(){
		return endOfGame;
	}
	
	//Also prime a variable holding which player it is
	public boolean allowBabylon7thChoice(){
		for (Map.Entry<PlayerMeta, PublicGameState> publicState : publicStates.entrySet()){
			if (publicState.getValue().babylonSeventhPlay){
				babylon7thPlayer = publicState.getKey();
				for (PlayerMeta player : getPlayers()){
					//executeLastCardActions() has already run by now
					//just need a non-null value for all of the other players to get the logic flow to work
					submittedPlays.put(player, CardPlayOptions.NOTHING);
				}
				submittedPlays.put(babylon7thPlayer, null);
				return true;
			}
		}
		return false;
	}
	
	public PlayerMeta whoCanUseBabylon7th(){
		return babylon7thPlayer;
	}
	
	public Card getBabylon7thCard(){
		return privateStates.get(babylon7thPlayer).hand.get(0);
	}
	
	//Also prime a variable holding which player it is
	public boolean playerBuiltHalikarnassosDiscardSearchStage(){
		for (Map.Entry<PlayerMeta, PublicGameState> publicState : publicStates.entrySet()){
			if (publicState.getValue().halikarnassosDiscardSearch){
				halikarnassosDiscardPlayer = publicState.getKey();
				for (PlayerMeta player : getPlayers()){
					//executeLastCardActions() has already run by now
					//just need a non-null value for all of the other players to get the logic flow to work
					submittedPlays.put(player, CardPlayOptions.NOTHING);
				}
				submittedPlays.put(halikarnassosDiscardPlayer, null);
				return true;
			}
		}
		return false;
	}
	
	public PlayerMeta whoGetsToSearchDiscardPile(){
		return halikarnassosDiscardPlayer;
	}
	
	public List<Card> getDiscard(){
		List<Card> filteredDiscard = Lists.newArrayList();
		PublicGameState publicState = getStateForPlayer(halikarnassosDiscardPlayer).getPublicState();
		for (Card card : discard){
			if (publicState.hasntBuiltCardYet(card)){
				filteredDiscard.add(card);
			}
		}
		return filteredDiscard;
	}
	
	public void addChatLine(String playerName, String text){
		chatLog.add(playerName + ": " + text);
	}
	
	public List<String> getChatLog(){
		return chatLog;
	}
	
	public static class PublicGameState {
		@Expose public PlayerMeta me;
		@Expose public PlayerMeta leftNeighbor;
		@Expose public PlayerMeta rightNeighbor;
		@Expose public int coins;
		@Expose public int coinsSpentTrading;
		public int coinsGivenTrading;
		public boolean possessesForum;
		public boolean possessesCaravansery;
		public boolean alexandriaMaterials;
		public boolean alexandriaGoods;
		public boolean babylonScienceSymbol;
		@Expose public boolean babylonSeventhPlay;
		public boolean halikarnassosDiscardSearch;
		@Expose public boolean olympiaCloneGuild;
		public boolean[] olympiaFreebieCharges;
		public int rhodosShields;
		@Expose public Wonder wonder;
		@Expose public final List<Integer> conflictTokens;
		@Expose private final Map<Card.Type, Set<Card>> builtCards;
		@Expose private final Map<PlayerMeta, List<ResourceCard>> resourcesForSale;
		@Expose public final Map<PlayerMeta, List<ResourceCard>> purchasedNeighborResources;
		@Expose public final List<ResourceType> purchasedNeighborWonderResources;
		public PublicGameState(PlayerMeta me, PlayerMeta leftNeighbor, PlayerMeta rightNeighbor)
		{
			this.me = me;
			this.leftNeighbor = leftNeighbor;
			this.rightNeighbor = rightNeighbor;
			coins = 3;
			coinsSpentTrading = 0;
			coinsGivenTrading = 0;
			conflictTokens = Lists.newArrayList();
			possessesForum = false;
			possessesCaravansery = false;
			alexandriaGoods = false;
			alexandriaMaterials = false;
			babylonScienceSymbol = false;
			babylonSeventhPlay = false;
			halikarnassosDiscardSearch = false;
			olympiaCloneGuild = false;
			olympiaFreebieCharges = new boolean[]{false, false, false};
			rhodosShields = 0;
			wonder = null;
			builtCards = Maps.newHashMap();
			for (Card.Type type : Card.Type.values()){
				builtCards.put(type, Sets.<Card>newHashSet());
			}
			resourcesForSale = Maps.newHashMap();
			resourcesForSale.put(leftNeighbor, Lists.<ResourceCard>newArrayList());
			resourcesForSale.put(rightNeighbor, Lists.<ResourceCard>newArrayList());
			purchasedNeighborResources = Maps.newHashMap();
			purchasedNeighborResources.put(leftNeighbor, Lists.<ResourceCard>newArrayList());
			purchasedNeighborResources.put(rightNeighbor, Lists.<ResourceCard>newArrayList());
			purchasedNeighborWonderResources = Lists.newArrayList();
		}
		
		public void addBuiltCard(Card card)
		{
			builtCards.get(card.getType()).add(card);
			if (halikarnassosDiscardSearch){
				halikarnassosDiscardSearch = false;
			}
			else if (olympiaFreebieCharges[card.getAge()-1] && !canCoverCost(card.getCost()))
			{
				olympiaFreebieCharges[card.getAge()-1] = false;
			}
			else
			{
				List<ResourceType> cost = card.getCost();
				if (cost.contains(ResourceType.COIN)){
					do{
						if (cost.remove(ResourceType.COIN)){
							coins--;
						}
						else{
							break;
						}
					}
					while (coins > 0);
				}
			}
		}
		
		public Collection<Card> getCards (Card.Type type)
		{
			return builtCards.get(type);
		}
		
		public void buildNextWonderStage(Card burntCard, PrivateGameState gameState){
			wonder.buildNextStage(burntCard, gameState);
		}
		
		public boolean hasntBuiltCardYet(Card card)
		{
			for (Card builtCard : getCards(card.getType())){
				if (builtCard.getName().equals(card.getName()))
				{
					return false;
				}
			}
			return true;
		}
		
		public void addResourceForSale(PlayerMeta neighbor, ResourceCard resource)
		{
			resourcesForSale.get(neighbor).add(resource);
		}
		
		private boolean hasPrerequisiteCard(Card card){
			for (Card ancestor : card.getAncestors()){
				if (builtCards.get(ancestor.getType()).contains(ancestor)){
					return true;
				}
			}
			return false;
		}
		
		public boolean hasResourcesToBuildCard(Card card)
		{
			if (olympiaFreebieCharges[card.getAge() - 1] || halikarnassosDiscardSearch){
				return true;
			}
			if (hasPrerequisiteCard(card)){
				return true;
			}
			List<ResourceType> cost = card.getCost();
			return canCoverCost(cost);
		}
		
		public boolean hasResourcesToBuildNextWonderStage()
		{
			WonderStage stage = wonder.getNextStage();
			if (stage == null)
			{
				return false;
			}
			List<ResourceType> cost = stage.getCost();
			return canCoverCost(cost);
		}
		
		public boolean canCoverCost(List<ResourceType> cost){
			List<Map<ResourceType, Integer>> variableResources = Lists.newArrayList();
			//Use resource from wonder board
			int indexOf = cost.indexOf(wonder.getResource());
			if (indexOf > -1){
				cost.remove(indexOf);
			}
			//Consider coins against coin cost
			for (int i = 0; i < coins; i++){
				indexOf = cost.indexOf(ResourceType.COIN);
				if (indexOf < 0){
					break;
				}
				else{
					cost.remove(indexOf);
				}
			}
			if (cost.isEmpty())
			{
				return true;
			}
			if (cost.contains(ResourceType.COIN)){
				//We don't have enough coins to satisfy the monetary cost
				return false;
			}
			//Use resources from cards and purchased resources
			List<Map<ResourceType, Integer>> resourcesAvailable = Lists.newArrayList();
			for (List<ResourceCard> purchasedResources : purchasedNeighborResources.values()){
				for (ResourceCard purchasedResource : purchasedResources){
					resourcesAvailable.add(purchasedResource.getValuesGiven());
				}
			}
			for (ResourceType wonderResource : purchasedNeighborWonderResources){
				Map<ResourceType, Integer> wonderResourceMap = Maps.newHashMap();
				wonderResourceMap.put(wonderResource, 1);
				resourcesAvailable.add(wonderResourceMap);
			}
			Collection<Card> resourceCards = Sets.newHashSet(getCards(Card.Type.MATERIAL));
			resourceCards.addAll(getCards(Card.Type.GOOD));
			for (Card card : resourceCards){
				ResourceCard resource = (ResourceCard) card;
				resourcesAvailable.add(resource.getValuesGiven());
			}
			//Split into simple and variable-choice resources
			for (Map<ResourceType, Integer> values : resourcesAvailable){
				if (values.keySet().size() > 1){
					//Goods cards shouldn't make it here
					variableResources.add(values);
				}
				else{
					ResourceType type = values.keySet().iterator().next(); //Should be guaranteed to have just one key
					if (cost.contains(type)){
						for (int i = 0; i < values.get(type); i++){ //Might be Age II resource that can fulfill two cost requirements
							indexOf = cost.indexOf(type);
							if (indexOf >= 0){
								cost.remove(indexOf);
							}
						}
					}
					if (cost.isEmpty())
					{
						return true; //Simple resources were enough
					}
				}
			}
			return canCoverCostUsingVariableResources(cost, variableResources);
		}
		
		private boolean canCoverCostUsingVariableResources(List<ResourceType> cost, List<Map<ResourceType, Integer>> variableResources){
			//Fortunately, no resource sources mix raw materials and goods, so we split the problem further. 
			//Furthermore, the only variable goods resources come from the full maps provided here, so solving the goods problem is trivial
			int goodsAvailable = 0;
			List<ResourceType> goodsCost = Lists.newArrayList();
			List<ResourceType> materialsCost = Lists.newArrayList();
			for (ResourceType costType : cost){
				if (costType == ResourceType.GLASS || costType == ResourceType.LOOM || costType == ResourceType.PAPYRUS){
					goodsCost.add(costType);
				}
				else{
					materialsCost.add(costType);
				}
			}
			Map<ResourceType, Integer> materialMap = Maps.newHashMap();
			materialMap.put(ResourceType.CLAY, 1);
			materialMap.put(ResourceType.STONE, 1);
			materialMap.put(ResourceType.ORE, 1);
			materialMap.put(ResourceType.LUMBER, 1);
			int fullMaterialsAvailable = 0;
			if (alexandriaMaterials){
				variableResources.add(materialMap);
				fullMaterialsAvailable++;
			}
			if (alexandriaGoods){
				goodsAvailable++;
			}
			if (possessesCaravansery){
				variableResources.add(materialMap);
				fullMaterialsAvailable++;
			}
			if (possessesForum){
				goodsAvailable++;
			}
			//Instead of using maps for the goods, we simply count the number of full-variable-goods-maps you have against the required goods cost
			if (goodsCost.size() > goodsAvailable){
				//Don't have enough for the manufactured goods, let alone the raw materials
				return false;
			}
			if (fullMaterialsAvailable >= materialsCost.size()){
				//Much more performant and reliable
				return true;
			}
			List<Map<ResourceType, Integer>> usefulResources = Lists.newArrayList();
			for (ResourceType type : materialsCost){
				boolean possibleToProduce = false;
				for (Map<ResourceType, Integer> variableResource : variableResources){
					if (variableResource.containsKey(type)){
						possibleToProduce = true;
						if (!usefulResources.contains(variableResource)){
							usefulResources.add(variableResource);
						}
					}
				}
				if (!possibleToProduce){
					return false;
				}
			}
			if (usefulResources.size() < materialsCost.size()){
				//No combination of choices will reach it
				return false;
			}
			return runMarriageProblem(usefulResources, materialsCost);
		}
		
		//Returns true if max flow from resources to cost is equal to number of cost nodes
		/*	      R - C
			     /     \
			S - V       T
			     \
			      R
			
		Where S=source, V=controls choosing just one of a group of N resources, R=all producible resources, C=cost (flow=1 from R->C, where R=C), T=sink
		*/
		private boolean runMarriageProblem(List<Map<ResourceType, Integer>> variableResources, List<ResourceType> cost){
			int graphSize = cost.size() + 2; // One vertex for each resource cost + source & sink vertices
			for (Map<ResourceType, Integer> variableResource : variableResources){
				graphSize++; //V-node; Flow 1 into every V from source
				graphSize += variableResource.size(); //All of the R-nodes; Flow 1 from V into each of its resources
			}
			int[][] graph = new int[graphSize][];
			for (int i = 0; i < graphSize; i++){
				graph[i] = new int[graphSize];
				for (int j = 0; j < graphSize; j++){
					graph[i][j] = 0;
				}
			}
			Map<ResourceType, List<Integer>> costIndex = Maps.newHashMap();
			costIndex.put(ResourceType.CLAY, Lists.<Integer>newArrayList());
			costIndex.put(ResourceType.LUMBER, Lists.<Integer>newArrayList());
			costIndex.put(ResourceType.ORE, Lists.<Integer>newArrayList());
			costIndex.put(ResourceType.STONE, Lists.<Integer>newArrayList());
			int cIndex = graphSize - 1 - cost.size(); 
			// shoving cost vertices to the end of the array
			// if graphSize = 10 and there are 2 cost nodes, costIndex should start at 7 (vertex 9 is the sink node)
			for (ResourceType costType : cost){
				costIndex.get(costType).add(cIndex);
				//Flows from cost nodes to sink node
				graph[cIndex][graphSize - 1] = 1;
				cIndex++;
			}
			int vIndex = 1; //index 0 belongs to source vertex
			for (Map<ResourceType, Integer> variableResource : variableResources){
				//Flows from source node to V-nodes
				graph[0][vIndex] = 1;
				int rIndex = vIndex + 1;
				for (ResourceType resource : variableResource.keySet()){ //Assuming each value in an entry = 1
					//Flows from V-node to resource nodes
					graph[vIndex][rIndex] = 1;
					//Flows from resource nodes to cost nodes
					for (Integer costVertex : costIndex.get(resource)){
						graph[rIndex][costVertex] = 1;
					}
					rIndex++;
				}
				vIndex++;
			}
			return MaxFlow.fordFulkerson(graph, 0, graphSize - 1) == cost.size();
		}
		
		public int getNumShields()
		{
			int total = 0;
			for (Card card : getCards(Card.Type.MILITARY)){
				total += ((MilitaryCard)card).getShields();
			}
			total += rhodosShields;
			return total;
		}
		
		//Helping out the complicated scientific algorithm at end-game time
		public boolean builtScientistsGuild()
		{
			for (Card card : getCards(Card.Type.GUILD)){
				if (card.getName().equals("Scientists Guild"))
				{
					return true;
				}
			}
			return false;
		}
	}
	
	public static class PrivateGameState {
		@Expose public final PlayerMeta me;
		@Expose public final Map<PlayerMeta, PublicGameState> publicStates;
		public int costPerLeftNeighborMaterial;
		public int costPerRightNeighborMaterial;
		public int costPerLeftNeighborGood;
		public int costPerRightNeighborGood;
		public final List<VictoryPointCountingAlgorithm> endGamePointAlgorithms;
		@Expose private Card lastCardPlayed;
		@Expose private List<Card> hand;
		public PrivateGameState(PlayerMeta me, List<PlayerMeta> players, Map<PlayerMeta, PublicGameState> publicStates)
		{
			this.me = me;
			this.publicStates = publicStates;
			costPerLeftNeighborMaterial = 2;
			costPerRightNeighborMaterial = 2;
			costPerLeftNeighborGood = 2;
			costPerRightNeighborGood = 2;
			//Add in default point-counting algorithms for all players
			//Commercial and Guild algorithms will get added as their cards are played
			endGamePointAlgorithms = Lists.newArrayList(
				Algorithms.endGameMilitary(),
				Algorithms.endGameTreasury(),
				Algorithms.endGameWonder(),
				Algorithms.endGameCivilian(),
				Algorithms.endGameScientific()
			);
			hand = Lists.newArrayList();
		}
		
		public PublicGameState getPublicState()
		{
			return publicStates.get(me);
		}
		
		public PublicGameState getLeftNeighborState()
		{
			return publicStates.get(getLeftNeighbor());
		}
		
		public PublicGameState getRightNeighborState()
		{
			return publicStates.get(getRightNeighbor());
		}
		
		public PlayerMeta getLeftNeighbor(){
			return publicStates.get(me).leftNeighbor;
		}
		
		public PlayerMeta getRightNeighbor(){
			return publicStates.get(me).rightNeighbor;
		}
		
		public void giveInitialHand(List<Card> hand){
			this.hand.addAll(hand);
		}
		
		public void tellNeighborsAboutResourceForSale(ResourceCard resource)
		{
			getLeftNeighborState().addResourceForSale(me, resource);
			getRightNeighborState().addResourceForSale(me, resource);
		}
		
		public void purchaseResource(PlayerMeta neighbor, ResourceCard resource)
		{
			PublicGameState myState = publicStates.get(me);
			if (publicStates.get(neighbor).hasntBuiltCardYet(resource) // My neighbor doesn't even have that card
				|| myState.purchasedNeighborResources.get(neighbor).contains(resource)) //I've already purchased that resource from that neighbor
			{
				return;
			}
			int goldCost = 0;
			if (neighbor.equals(getLeftNeighbor()) && resource.getType() == Card.Type.MATERIAL)
			{
				goldCost = costPerLeftNeighborMaterial;
			}
			else if (neighbor.equals(getLeftNeighbor()) && resource.getType() == Card.Type.GOOD)
			{
				goldCost = costPerLeftNeighborGood;
			}
			else if (neighbor.equals(getRightNeighbor()) && resource.getType() == Card.Type.GOOD)
			{
				goldCost = costPerRightNeighborGood;
			}
			else if (neighbor.equals(getRightNeighbor()) && resource.getType() == Card.Type.MATERIAL)
			{
				goldCost = costPerRightNeighborMaterial;
			}
			if (myState.coins - publicStates.get(me).coinsSpentTrading >= goldCost) //No notion of debt in this game
			{
				publicStates.get(me).coinsSpentTrading += goldCost;
				publicStates.get(neighbor).coinsGivenTrading += goldCost;
				myState.purchasedNeighborResources.get(neighbor).add(resource);
			}
		}
		
		public void purchaseWonderResource(PlayerMeta neighbor, ResourceType resource)
		{
			PublicGameState myState = publicStates.get(me);
			if (publicStates.get(neighbor).wonder.getResource() != resource //My neighbor's wonder doesn't even provide that resource
				|| myState.purchasedNeighborWonderResources.contains(resource)) //I've already purchased that resource from my neighbor
			{
				return;
			}
			int goldCost = 0;
			boolean isGood = resource == ResourceType.GLASS || resource == ResourceType.LOOM || resource == ResourceType.PAPYRUS;
			if (neighbor.equals(getLeftNeighbor()) && !isGood)
			{
				goldCost = costPerLeftNeighborMaterial;
			}
			else if (neighbor.equals(getLeftNeighbor()) && isGood)
			{
				goldCost = costPerLeftNeighborGood;
			}
			else if (neighbor.equals(getRightNeighbor()) && isGood)
			{
				goldCost = costPerRightNeighborGood;
			}
			else if (neighbor.equals(getRightNeighbor()) && !isGood)
			{
				goldCost = costPerRightNeighborMaterial;
			}
			if (myState.coins - publicStates.get(me).coinsSpentTrading >= goldCost) //No notion of debt in this game
			{
				publicStates.get(me).coinsSpentTrading += goldCost;
				publicStates.get(neighbor).coinsGivenTrading += goldCost;
				myState.purchasedNeighborWonderResources.add(resource);
			}
		}
		
		public void executeLastCardAction(CardPlayOptions play){
			getPublicState().coins -= getPublicState().coinsSpentTrading;
			getPublicState().coins += getPublicState().coinsGivenTrading;
			getPublicState().coinsSpentTrading = 0;
			getPublicState().coinsGivenTrading = 0;
			switch (play){
			case BUILD:
				getPublicState().addBuiltCard(lastCardPlayed);
				switch (lastCardPlayed.getType()){
					case MATERIAL:
					case GOOD:
						tellNeighborsAboutResourceForSale((ResourceCard)lastCardPlayed);
						break;
					case COMMERCIAL:
						((CommercialCard)lastCardPlayed).alterGameState(this);
						break;
					case GUILD:
						((GuildCard)lastCardPlayed).addVictoryPointAlgorithms(this);
						break;
				}
				break;
			case WONDER:
				getPublicState().wonder.buildNextStage(lastCardPlayed, this);
				break;
			case DISCARD:
				getPublicState().coins += 3;
				break;
			case SELECT:
			case NOTHING:
			default:
				break;
			}
			lastCardPlayed = null;
		}
		
		public Integer countTotalScore(){
			Integer total = 0;
			for (VictoryPointCountingAlgorithm endGamePointAlgorithm : endGamePointAlgorithms){
				total += endGamePointAlgorithm.numVictoryPoints(this);
			}
			return total;
		}
	}
}

