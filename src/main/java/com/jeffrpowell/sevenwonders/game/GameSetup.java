package com.jeffrpowell.sevenwonders.game;

import com.google.common.base.Function;
import com.google.common.collect.Maps;
import com.jeffrpowell.sevenwonders.PlayerMeta;
import com.jeffrpowell.sevenwonders.lobby.NewGameState;
import java.util.List;
import java.util.Map;

public class GameSetup {
	private final List<PlayerMeta> players;
	private final Map<PlayerMeta, Boolean> playerConnected;
	private final boolean randomWonders;
	private final Map<PlayerMeta, String> wonderChoices;
	
	public GameSetup(List<PlayerMeta> players, boolean randomWonders, Map<PlayerMeta, String> wonderChoices)
	{
		this.players = players;
		this.playerConnected = Maps.newHashMap();
		for (PlayerMeta player : players){
			playerConnected.put(player, false);
		}
		this.randomWonders = randomWonders;
		this.wonderChoices = wonderChoices;
	}
	
	public static GameSetup fromNewGameState(final NewGameState newGame, List<PlayerMeta> players)
	{
		Map<PlayerMeta, String> wonderChoices = Maps.toMap(players, new Function<PlayerMeta, String>(){
			@Override
			public String apply(PlayerMeta f){
				return newGame.getWonderChoices().get(f.getUsername());
			}
		});
		return new GameSetup(players, newGame.isRandomWonders(), wonderChoices);
	}
	
	public List<PlayerMeta> getPlayers()
	{
		return players;
	}
	
	public boolean allPlayersConnected()
	{
		return !playerConnected.containsValue(false);
	}
	
	public void setPlayerConnected(PlayerMeta player, boolean connected)
	{
		playerConnected.put(player, connected);
	}

	public boolean isRandomWonders(){
		return randomWonders;
	}

	public Map<PlayerMeta, String> getWonderChoices(){
		return wonderChoices;
	}
	
}
