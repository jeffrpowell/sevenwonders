package com.jeffrpowell.sevenwonders.game.messages;

import com.google.gson.annotations.Expose;
import com.jeffrpowell.sevenwonders.Message;
import com.jeffrpowell.sevenwonders.PlayerMeta;
import com.jeffrpowell.sevenwonders.game.Game.PrivateGameState;

public class GameStateMessage extends Message{
	private static final ID id = ID.GAME_STATE;
	@Expose private final PrivateGameState gameState;
	public GameStateMessage(PlayerMeta player, PrivateGameState gameState)
	{
		super(id, player);
		this.gameState = gameState;
	}
	
	public PrivateGameState getGameState(){
		return gameState;
	}
}
