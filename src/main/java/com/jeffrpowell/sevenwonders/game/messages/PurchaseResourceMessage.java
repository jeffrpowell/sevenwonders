package com.jeffrpowell.sevenwonders.game.messages;

import com.google.common.base.Strings;
import com.jeffrpowell.sevenwonders.DefaultMessage;
import com.jeffrpowell.sevenwonders.Message;
import com.jeffrpowell.sevenwonders.PlayerMeta;
import com.jeffrpowell.sevenwonders.cards.CardFactory;
import com.jeffrpowell.sevenwonders.cards.ResourceCard;
import java.util.Map;

public class PurchaseResourceMessage extends Message{
	private static final ID id = ID.PURCHASE_RESOURCE;
	private final ResourceCard resource;
	private final String seller;

	public PurchaseResourceMessage(PlayerMeta buyer, ResourceCard resource, String seller){
		super(id, buyer);
		this.resource = resource;
		this.seller = seller;
	}

	public ResourceCard getResource(){
		return resource;
	}

	public String getSeller(){
		return seller;
	}

	public static PurchaseResourceMessage fromMap(Map<String, Object> map) throws IllegalArgumentException
	{
		Message meta = DefaultMessage.fromMap(map);
		ResourceCard resource = (ResourceCard)CardFactory.fromMap((Map<String, Object>)map.get("resource"));
		String seller = (String)map.get("seller");
		if (Strings.isNullOrEmpty(seller))
		{
			throw new IllegalArgumentException("Need a valid player in the 'seller' key");
		}
		return new PurchaseResourceMessage(meta.getPlayer(), resource, seller);
	}
}
