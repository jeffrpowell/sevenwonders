package com.jeffrpowell.sevenwonders.game.messages;

import com.jeffrpowell.sevenwonders.DefaultMessage;
import com.jeffrpowell.sevenwonders.Message;
import com.jeffrpowell.sevenwonders.PlayerMeta;
import com.jeffrpowell.sevenwonders.cards.Card;
import com.jeffrpowell.sevenwonders.cards.CardFactory;
import com.jeffrpowell.sevenwonders.game.Game.CardPlayOptions;
import java.util.Map;

public class PlayCardMessage extends Message{
	private static final ID id = ID.PLAY_CARD;
	private final CardPlayOptions play;
	private final Card card;

	public PlayCardMessage(PlayerMeta player, CardPlayOptions play, Card card){
		super(id, player);
		this.play = play;
		this.card = card;
	}

	public CardPlayOptions getPlay(){
		return play;
	}

	public Card getCard(){
		return card;
	}
	public static PlayCardMessage fromMap(Map<String, Object> map) throws IllegalArgumentException
	{
		Message meta = DefaultMessage.fromMap(map);
		CardPlayOptions play = CardPlayOptions.valueOf((String)map.get("play"));
		Card card = CardFactory.fromMap((Map<String, Object>)map.get("card"));
		return new PlayCardMessage(meta.getPlayer(), play, card);
	}
}
