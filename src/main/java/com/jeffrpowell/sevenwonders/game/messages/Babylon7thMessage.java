package com.jeffrpowell.sevenwonders.game.messages;

import com.jeffrpowell.sevenwonders.Message;
import com.jeffrpowell.sevenwonders.PlayerMeta;
import com.jeffrpowell.sevenwonders.cards.Card;
import com.jeffrpowell.sevenwonders.game.Game.CardPlayOptions;
import java.util.Collection;

public class Babylon7thMessage extends Message{
	private static final ID id = ID.BABYLON_PLAY;
	private final Collection<CardPlayOptions> options;
	private final Card card;

	public Babylon7thMessage(PlayerMeta player, Collection<CardPlayOptions> options, Card card){
		super(id, player);
		this.options = options;
		this.card = card;
	}
}
