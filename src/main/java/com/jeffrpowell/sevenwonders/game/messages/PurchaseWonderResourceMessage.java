package com.jeffrpowell.sevenwonders.game.messages;

import com.google.common.base.Strings;
import com.jeffrpowell.sevenwonders.DefaultMessage;
import com.jeffrpowell.sevenwonders.Message;
import com.jeffrpowell.sevenwonders.PlayerMeta;
import com.jeffrpowell.sevenwonders.ResourceType;
import java.util.Map;

public class PurchaseWonderResourceMessage extends Message{
	private static final ID id = ID.PURCHASE_RESOURCE;
	private final ResourceType resource;
	private final String seller;

	public PurchaseWonderResourceMessage(PlayerMeta buyer, ResourceType resource, String seller){
		super(id, buyer);
		this.resource = resource;
		this.seller = seller;
	}

	public ResourceType getResource(){
		return resource;
	}

	public String getSeller(){
		return seller;
	}

	public static PurchaseWonderResourceMessage fromMap(Map<String, Object> map) throws IllegalArgumentException
	{
		Message meta = DefaultMessage.fromMap(map);
		ResourceType resource = ResourceType.valueOf((String)map.get("resource"));
		String seller = (String)map.get("seller");
		if (Strings.isNullOrEmpty(seller))
		{
			throw new IllegalArgumentException("Need a valid player in the 'seller' key");
		}
		return new PurchaseWonderResourceMessage(meta.getPlayer(), resource, seller);
	}
}
