package com.jeffrpowell.sevenwonders.game.messages;

import com.google.gson.annotations.Expose;
import com.jeffrpowell.sevenwonders.Message;
import com.jeffrpowell.sevenwonders.PlayerMeta;
import com.jeffrpowell.sevenwonders.game.Scorecard;

public class ScoreMessage extends Message{
	private static final ID id = ID.SCORE;
	@Expose private final Scorecard scorecard;
	
	public ScoreMessage(PlayerMeta player, Scorecard scorecard){
		super(id, player);
		this.scorecard = scorecard;
	}
	
	public Scorecard getScorecard(){
		return scorecard;
	}
}
