package com.jeffrpowell.sevenwonders.game.messages;

import com.google.gson.annotations.Expose;
import com.jeffrpowell.sevenwonders.Message;
import com.jeffrpowell.sevenwonders.PlayerMeta;
import com.jeffrpowell.sevenwonders.game.Game;

public class ExceptionMessage extends Message{
	private static final ID id = ID.ERROR;
	@Expose private final String msg;
	@Expose private final String request;
	@Expose private final String exceptionMessage;
	@Expose private final String[] stackTrace;
	@Expose private final Game.PrivateGameState privateGameState;

	public ExceptionMessage(PlayerMeta player, String msg, Throwable throwable, String request, Game.PrivateGameState privateGameState){
		super(id, player);
		this.msg = msg;
		this.request = request;
		this.exceptionMessage = throwable.getMessage();
		StackTraceElement[] traceElements = throwable.getStackTrace();
		this.stackTrace = new String[traceElements.length];
		for (int i = 0; i < traceElements.length; i++){
			stackTrace[i] = traceElements[i].toString();
		}
		this.privateGameState = privateGameState;
	}

	public String getMsg(){
		return msg;
	}
	
	public String getRequest(){
		return request;
	}

	public Game.PrivateGameState getPrivateGameState(){
		return privateGameState;
	}
	
}
