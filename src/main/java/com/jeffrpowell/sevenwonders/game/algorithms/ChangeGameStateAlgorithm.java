package com.jeffrpowell.sevenwonders.game.algorithms;

import com.jeffrpowell.sevenwonders.game.Game;

public interface ChangeGameStateAlgorithm{
	public void alterGameState(Game.PrivateGameState gameState);
}
