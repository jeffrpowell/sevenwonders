package com.jeffrpowell.sevenwonders.game.algorithms;

import com.jeffrpowell.sevenwonders.game.Game;

public interface VictoryPointCountingAlgorithm{
	public int numVictoryPoints(Game.PrivateGameState gameState);
}

	
