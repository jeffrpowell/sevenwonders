package com.jeffrpowell.sevenwonders.game.algorithms;

import com.google.common.collect.Maps;
import com.jeffrpowell.sevenwonders.cards.Card;
import com.jeffrpowell.sevenwonders.cards.CivilianCard;
import com.jeffrpowell.sevenwonders.cards.ScientificCard;
import com.jeffrpowell.sevenwonders.game.Game;
import java.util.Collection;
import java.util.Map;

public class Algorithms
{
	private Algorithms(){}
	
	public static ChangeGameStateAlgorithm noOpChangeAlgorithm(){
		return new ChangeGameStateAlgorithm(){
			@Override
			public void alterGameState(Game.PrivateGameState gameState)
			{}
		};
	}
	
	public static ChangeGameStateAlgorithm cheaperResourcesAlgorithm(final boolean rawMaterials, final boolean left){
		return new ChangeGameStateAlgorithm(){
			@Override
			public void alterGameState(Game.PrivateGameState gameState)
			{
				if (rawMaterials && left)
					gameState.costPerLeftNeighborMaterial = 1;
				else if (rawMaterials && !left)
					gameState.costPerRightNeighborMaterial = 1;
				else if (!rawMaterials)
				{
					//Only Marketplace does manufactured goods, and it applies to both neighbors
					gameState.costPerLeftNeighborGood = 1;
					gameState.costPerRightNeighborGood = 1;
				}
			}
		};
	}
	
	public static ChangeGameStateAlgorithm gainGoldAlgorithm(final int amount, final Card.Type cardType, final boolean countNeighbors){
		return new ChangeGameStateAlgorithm(){
			@Override
			public void alterGameState(Game.PrivateGameState gameState)
			{
				Game.PublicGameState publicState = gameState.getPublicState();
				if (cardType == null)
				{
					publicState.coins += amount;
				}
				else
				{
					publicState.coins += amount * publicState.getCards(cardType).size();
					if (countNeighbors)
					{
						publicState.coins += amount * (gameState.getLeftNeighborState().getCards(cardType).size() + 
													   gameState.getRightNeighborState().getCards(cardType).size());
					}
				}
			}
		};
	}
	
	public static ChangeGameStateAlgorithm gainGoldFromMyWonderStages(final int amount){
		return new ChangeGameStateAlgorithm(){
			@Override
			public void alterGameState(Game.PrivateGameState gameState)
			{
				gameState.getPublicState().coins += amount * gameState.getPublicState().wonder.numBuiltStages();
			}
		};
	}
	
	public static ChangeGameStateAlgorithm additionalResourcesAlgorithm(final boolean isForum){
		return new ChangeGameStateAlgorithm(){
			@Override
			public void alterGameState(Game.PrivateGameState gameState)
			{
				if (isForum)
				{
					gameState.getPublicState().possessesForum = true;
				}
				else
				{
					gameState.getPublicState().possessesCaravansery = true;
				}
			}
		};
	}
	
	public static ChangeGameStateAlgorithm addVictoryPointAlgorithm(final VictoryPointCountingAlgorithm algorithm){
		return new ChangeGameStateAlgorithm()
		{
			@Override
			public void alterGameState(Game.PrivateGameState gameState)
			{
				gameState.endGamePointAlgorithms.add(algorithm);
			}
		};
	}
	
	public static ChangeGameStateAlgorithm addRhodosShieldsAlgorithm(final int shields){
		return new ChangeGameStateAlgorithm()
		{
			@Override
			public void alterGameState(Game.PrivateGameState gameState)
			{
				gameState.getPublicState().rhodosShields += shields;
			}
		};
	}
	
	public static ChangeGameStateAlgorithm enableAlexandriaGoods(){
		return new ChangeGameStateAlgorithm()
		{
			@Override
			public void alterGameState(Game.PrivateGameState gameState)
			{
				gameState.getPublicState().alexandriaGoods = true;
			}
		};
	}
	
	public static ChangeGameStateAlgorithm enableAlexandriaMaterials(){
		return new ChangeGameStateAlgorithm()
		{
			@Override
			public void alterGameState(Game.PrivateGameState gameState)
			{
				gameState.getPublicState().alexandriaMaterials = true;
			}
		};
	}
	
	public static ChangeGameStateAlgorithm enableBabylonScienceSymbol(){
		return new ChangeGameStateAlgorithm()
		{
			@Override
			public void alterGameState(Game.PrivateGameState gameState)
			{
				gameState.getPublicState().babylonScienceSymbol = true;
			}
		};
	}
	
	public static ChangeGameStateAlgorithm enableBabylonSeventhPlay(){
		return new ChangeGameStateAlgorithm()
		{
			@Override
			public void alterGameState(Game.PrivateGameState gameState)
			{
				gameState.getPublicState().babylonSeventhPlay = true;
			}
		};
	}
	
	public static ChangeGameStateAlgorithm enableHalikarnassosDiscardSearch(){
		return new ChangeGameStateAlgorithm()
		{
			@Override
			public void alterGameState(Game.PrivateGameState gameState)
			{
				gameState.getPublicState().halikarnassosDiscardSearch = true;
			}
		};
	}
	
	public static ChangeGameStateAlgorithm enableOlympiaCloneGuild(){
		return new ChangeGameStateAlgorithm()
		{
			@Override
			public void alterGameState(Game.PrivateGameState gameState)
			{
				gameState.getPublicState().olympiaCloneGuild = true;
			}
		};
	}
	
	public static ChangeGameStateAlgorithm enableOlympiaFreebies(){
		return new ChangeGameStateAlgorithm()
		{
			@Override
			public void alterGameState(Game.PrivateGameState gameState)
			{
				gameState.getPublicState().olympiaFreebieCharges = new boolean[]{true, true, true};
			}
		};
	}
	
	public static VictoryPointCountingAlgorithm pointsForWonderStages(final int points, final boolean includeNeighbors){
		return new VictoryPointCountingAlgorithm(){
			@Override
			public int numVictoryPoints(Game.PrivateGameState gameState)
			{
				int total = points * gameState.getPublicState().wonder.numBuiltStages();
				if (includeNeighbors)
				{
					total += points * (gameState.getLeftNeighborState().wonder.numBuiltStages() + 
									   gameState.getRightNeighborState().wonder.numBuiltStages());
				}
				return total;
			}
		};
	}
	
	public static VictoryPointCountingAlgorithm pointsForMyCards(final int points, final Card.Type cardType){
		return new VictoryPointCountingAlgorithm(){
			@Override
			public int numVictoryPoints(Game.PrivateGameState gameState)
			{
				return points * gameState.getPublicState().getCards(cardType).size();
			}
		};
	}
	
	public static VictoryPointCountingAlgorithm pointsForNeighborsCards(final int points, final Card.Type cardType){
		return new VictoryPointCountingAlgorithm(){
			@Override
			public int numVictoryPoints(Game.PrivateGameState gameState)
			{
				return points * (gameState.getLeftNeighborState().getCards(cardType).size() + 
								 gameState.getRightNeighborState().getCards(cardType).size());
			}
		};
	}
	
	public static VictoryPointCountingAlgorithm pointsForNeighborsMilitaryLosses(final int points){
		return new VictoryPointCountingAlgorithm(){
			@Override
			public int numVictoryPoints(Game.PrivateGameState gameState)
			{
				int total = 0;
				for (Integer conflictToken : gameState.getLeftNeighborState().conflictTokens)
				{
					if (conflictToken == -1)
					{
						total += points;
					}
				}
				for (Integer conflictToken : gameState.getRightNeighborState().conflictTokens)
				{
					if (conflictToken == -1)
					{
						total += points;
					}
				}
				return total;
			}
		};
	}
	
	public static VictoryPointCountingAlgorithm endGameMilitary(){
		return new VictoryPointCountingAlgorithm(){
			@Override
			public int numVictoryPoints(Game.PrivateGameState gameState)
			{
				int total = 0;
				for (Integer conflictToken : gameState.getPublicState().conflictTokens)
				{
					total += conflictToken;
				}
				return total;
			}
		};
	}
	
	public static VictoryPointCountingAlgorithm endGameTreasury(){
		return new VictoryPointCountingAlgorithm(){
			@Override
			public int numVictoryPoints(Game.PrivateGameState gameState)
			{
				int coins = gameState.getPublicState().coins;
				return (coins - (coins % 3)) / 3;
			}
		};
	}
	
	public static VictoryPointCountingAlgorithm endGameWonder(){
		return new VictoryPointCountingAlgorithm(){
			@Override
			public int numVictoryPoints(Game.PrivateGameState gameState)
			{
				return gameState.getPublicState().wonder.getEarnedVictoryPoints();
			}
		};
	}
	
	public static VictoryPointCountingAlgorithm endGameCivilian(){
		return new VictoryPointCountingAlgorithm(){
			@Override
			public int numVictoryPoints(Game.PrivateGameState gameState)
			{
				int total = 0;
				for (Card card : gameState.getPublicState().getCards(Card.Type.CIVILIAN))
				{
					CivilianCard civCard = (CivilianCard) card;
					total += civCard.getVictoryPoints();
				}
				return total;
			}
		};
	}
	
	public static VictoryPointCountingAlgorithm endGameScientific(){
		return new VictoryPointCountingAlgorithm(){
			@Override
			public int numVictoryPoints(Game.PrivateGameState gameState)
			{
				Collection<Card> cards = gameState.getPublicState().getCards(Card.Type.SCIENTIFIC);
				Map<ScientificCard.Symbol, Integer> symbols = Maps.newHashMap();
				for (ScientificCard.Symbol symbol : ScientificCard.Symbol.values()){
					symbols.put(symbol, 0);
				}
				for (Card card : cards){
					ScientificCard.Symbol symbol = ((ScientificCard)card).getSymbol();
					symbols.put(symbol, symbols.get(symbol)+1);
				}
				int variableSymbols = gameState.getPublicState().builtScientistsGuild() ? 1 : 0;
				variableSymbols += gameState.getPublicState().babylonScienceSymbol ? 1 : 0;
				return findBestTotal(symbols, variableSymbols);
			}
			
			private int findBestTotal(Map<ScientificCard.Symbol, Integer> symbols, int variableSymbols)
			{
				int bestTotal = Integer.MIN_VALUE;
				switch (variableSymbols){
				case 2:
					for (ScientificCard.Symbol symbol1 : symbols.keySet())
					{
						symbols.put(symbol1, symbols.get(symbol1)+1);
						for (ScientificCard.Symbol symbol2 : symbols.keySet())
						{
							symbols.put(symbol2, symbols.get(symbol2)+1);
							int total = calculatePoints(symbols);
							if (total > bestTotal)
							{
								bestTotal = total;
							}
							symbols.put(symbol2, symbols.get(symbol2)-1);
						}
						symbols.put(symbol1, symbols.get(symbol1)-1);
					}
					break;
				case 1:
					for (ScientificCard.Symbol symbol : symbols.keySet())
					{
						symbols.put(symbol, symbols.get(symbol)+1);
						int total = calculatePoints(symbols);
						if (total > bestTotal)
						{
							bestTotal = total;
						}
						symbols.put(symbol, symbols.get(symbol)-1);
					}
					break;
				default:
					bestTotal = calculatePoints(symbols);
					break;
				}
				return bestTotal;
			}

			private int calculatePoints(Map<ScientificCard.Symbol, Integer> symbols)
			{
				int total = 0;
				boolean doneLooping = false;
				//Points for sets of identical symbols
				for (Integer value : symbols.values()){
					total += value * value;
					if (value == 0)
					{
						doneLooping = true; //piggy-backing on our introspective loop for the next phase of points
					}
				}
				//Points for sets of 3 different symbols
				while (!doneLooping){
					for (ScientificCard.Symbol symbol : symbols.keySet())
					{
						int newValue = symbols.get(symbol) - 1;
						symbols.put(symbol, newValue);
						if (newValue == 0)
						{
							doneLooping = true;
						}
					}
					total += 7;
				}
				return total;
			}
		};
	}
}
