package com.jeffrpowell.sevenwonders.game;

import com.jeffrpowell.sevenwonders.PlayerMeta;
import com.jeffrpowell.sevenwonders.cards.Card;
import com.jeffrpowell.sevenwonders.game.Game.CardPlayOptions;
import java.util.List;
import java.util.Set;

public interface GameListener {
	public void error(PlayerMeta player, String error);
	public void gameStateChanged(PlayerMeta player, Game.PrivateGameState game);
	public void giveCardOptions(PlayerMeta player, Set<CardPlayOptions> options);
	public void sendBabylon7th(PlayerMeta player, Card card, Set<CardPlayOptions> options);
	public void sendDiscardPile(PlayerMeta player, List<Card> discard);
	public void sendEndPlayResults(PlayerMeta player, Scorecard scorecard);
	public void chatMessage(PlayerMeta player, List<String> chatLog);
}
