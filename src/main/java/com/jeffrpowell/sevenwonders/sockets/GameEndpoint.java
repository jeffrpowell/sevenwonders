package com.jeffrpowell.sevenwonders.sockets;

import com.jeffrpowell.sevenwonders.CollectionMessage;
import com.jeffrpowell.sevenwonders.DefaultMessage;
import com.jeffrpowell.sevenwonders.HandshakeMessage;
import com.jeffrpowell.sevenwonders.JsonUtils;
import com.jeffrpowell.sevenwonders.Message;
import com.jeffrpowell.sevenwonders.Message.ID;
import com.jeffrpowell.sevenwonders.PlayerMeta;
import com.jeffrpowell.sevenwonders.SingleStringMessage;
import com.jeffrpowell.sevenwonders.cards.Card;
import com.jeffrpowell.sevenwonders.game.Game;
import com.jeffrpowell.sevenwonders.game.Game.CardPlayOptions;
import com.jeffrpowell.sevenwonders.game.GameController;
import com.jeffrpowell.sevenwonders.game.GameListener;
import com.jeffrpowell.sevenwonders.game.GameSetup;
import com.jeffrpowell.sevenwonders.game.Scorecard;
import com.jeffrpowell.sevenwonders.game.messages.Babylon7thMessage;
import com.jeffrpowell.sevenwonders.game.messages.ExceptionMessage;
import com.jeffrpowell.sevenwonders.game.messages.GameStateMessage;
import com.jeffrpowell.sevenwonders.game.messages.ScoreMessage;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/game")
public class GameEndpoint {
	private static final Listener listener;
	private static final GameController controller;
	static
	{
		listener = new Listener();
		controller = new GameController();
		controller.addListener(listener);
	}
	
	public static Set<PlayerMeta> getConnectedPlayers()
	{
		return listener.getConnectedPlayers();
	}
	
	@OnOpen
	public void onOpen(Session session)
	{
	}
	@OnClose
	public void onClose(Session session)
	{
		PlayerMeta player = listener.removePlayer(session);
		controller.removePlayer(player);
	}
	@OnMessage
	public void onMessage(Session session, String msg, boolean last)
	{
		try
		{
			Map<String, Object> message = JsonUtils.parseMessage(msg);
			if (message != null)
			{
				if (message.get("id").equals(Message.ID.HANDSHAKE.toString()))
				{
					listener.addPlayer(session, HandshakeMessage.fromMap(message));
					controller.handleMessage(message);
				}
				else if (listener.isValidPlayer(session, DefaultMessage.fromMap(message)))
				{
					controller.handleMessage(message);
				}
			}
		}
		catch (Exception e){
			PlayerMeta player = listener.getPlayerFromSession(session);
			listener.exception(new ExceptionMessage(player, "EXCEPTION", e, msg, controller.getPrivateGameState(player)));
		}
	}
	
	public static int startNewGame(GameSetup gameOptions)
	{
		return controller.startNewGame(gameOptions);
	}
	
	private static class Listener implements GameListener
	{
		private final static Map<PlayerMeta, Session> playerSessions = Collections.synchronizedMap(new HashMap<PlayerMeta, Session>());
		
		private void sendMessageOut(Message message)
		{
			try
			{
				String json = JsonUtils.exposedObjectToJson(message);
				playerSessions.get(message.getPlayer()).getAsyncRemote().sendText(json);
			}
			catch (Exception e){
				System.out.println(e.getMessage());
			}
		}
		
		public boolean isValidPlayer(Session session, Message message)
		{
			//Will check that the username and sessionId match to what we expect
			boolean valid = playerSessions.containsKey(message.getPlayer());
			if (!valid)
			{
				sendMessageOut(new SingleStringMessage(Message.ID.ERROR, message.getPlayer(), "Bad username/sessionId. Ignoring message."));
				return false;
			}
			return true;
		}
		
		public void addPlayer(Session session, HandshakeMessage message)
		{
			playerSessions.put(message.getPlayer(), session);
		}
		
		public PlayerMeta removePlayer(Session session)
		{
			PlayerMeta player = getPlayerFromSession(session);
			playerSessions.remove(player);
			return player;
		}
		
		public Set<PlayerMeta> getConnectedPlayers()
		{
			return playerSessions.keySet();
		}
		
		public PlayerMeta getPlayerFromSession(Session session)
		{
			for (PlayerMeta player : playerSessions.keySet()){
				if (playerSessions.get(player).getId().equals(session.getId()))
				{
					return player;
				}
			}
			return null;
		}
		
		public void exception(ExceptionMessage message){
			sendMessageOut(message);
		}

		@Override
		public void gameStateChanged(PlayerMeta player, Game.PrivateGameState game){
			sendMessageOut(new GameStateMessage(player, game));
		}
		
		@Override
		public void error(PlayerMeta player, String error)
		{
			sendMessageOut(new SingleStringMessage(ID.ERROR, player, error));
		}
		
		@Override
		public void giveCardOptions(PlayerMeta player, Set<CardPlayOptions> options)
		{
			sendMessageOut(new CollectionMessage(ID.CARD_PLAY_OPTIONS, player, options));
		}
		
		@Override
		public void sendEndPlayResults(PlayerMeta player, Scorecard scorecard){
			sendMessageOut(new ScoreMessage(player, scorecard));
		}

		@Override
		public void sendDiscardPile(PlayerMeta player, List<Card> discard){
			sendMessageOut(new CollectionMessage(ID.SEARCH_DISCARD, player, discard));
		}

		@Override
		public void sendBabylon7th(PlayerMeta player, Card card, Set<CardPlayOptions> options){
			sendMessageOut(new Babylon7thMessage(player, options, card));
		}
		
		@Override
		public void chatMessage(PlayerMeta player, List<String> chatLog){
			sendMessageOut(new CollectionMessage(ID.NEW_CHAT_MSG, player, chatLog));
		}
	}
}
