package com.jeffrpowell.sevenwonders.sockets;

import com.jeffrpowell.sevenwonders.DefaultMessage;
import com.jeffrpowell.sevenwonders.HandshakeMessage;
import com.jeffrpowell.sevenwonders.JsonUtils;
import com.jeffrpowell.sevenwonders.Message;
import com.jeffrpowell.sevenwonders.Message.ID;
import com.jeffrpowell.sevenwonders.PlayerMeta;
import com.jeffrpowell.sevenwonders.SingleStringMessage;
import com.jeffrpowell.sevenwonders.lobby.LobbyController;
import com.jeffrpowell.sevenwonders.lobby.LobbyListener;
import com.jeffrpowell.sevenwonders.lobby.LobbyState;
import com.jeffrpowell.sevenwonders.lobby.NewGameState;
import com.jeffrpowell.sevenwonders.lobby.messages.LobbyStateMessage;
import com.jeffrpowell.sevenwonders.lobby.messages.NewGameStateMessage;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/lobby")
public class LobbyEndpoint {
	private static final LobbyController controller;
	private static final Listener listener;
	static
	{
		controller = new LobbyController();
		listener = new Listener();
		controller.addListener(listener);
	}
	
	public static Set<PlayerMeta> getConnectedPlayers()
	{
		return listener.getConnectedPlayers();
	}
	
	@OnOpen
	public void onOpen(Session player)
	{
	}
	@OnClose
	public void onClose(Session player)
	{
		listener.removePlayer(player);
	}
	@OnMessage
	public void onMessage(Session session, String msg, boolean last)
	{
		System.out.println("Message: " + msg);
		Map<String, Object> message = JsonUtils.parseMessage(msg);
		if (message != null)
		{
			if (message.get("id").equals(Message.ID.HANDSHAKE.toString()))
			{
				listener.addPlayer(session, HandshakeMessage.fromMap(message), controller.getLobbyState());
			}
			else if (listener.isValidPlayer(session, DefaultMessage.fromMap(message)))
			{
				controller.handleMessage(message);
			}
		}
	}
	
	private static class Listener implements LobbyListener
	{
		private final static Map<PlayerMeta, Session> playerSessions = Collections.synchronizedMap(new HashMap<PlayerMeta, Session>());
		
		private void sendMessageOut(Session session, Message message)
		{
			session.getAsyncRemote().sendText(JsonUtils.objectToJson(message));
		}

		@Override
		public void lobbyStateChanged(LobbyState state){
			for (Session player : playerSessions.values()){
				sendMessageOut(player, new LobbyStateMessage(getPlayerFromSession(player), state));
			}
		}

		@Override
		public void gameStateChanged(List<PlayerMeta> players, NewGameState game){
			for (PlayerMeta player : players){
				sendMessageOut(playerSessions.get(player), NewGameStateMessage.updating(player, game));
			}
		}
		
		@Override
		public void error(PlayerMeta player, String error)
		{
			sendMessageOut(playerSessions.get(player), new SingleStringMessage(Message.ID.ERROR, player, error));
		}
		
		@Override
		public void gameRedirect(List<PlayerMeta> players){
			for (PlayerMeta player : players){
				sendMessageOut(playerSessions.get(player), new DefaultMessage(ID.START_GAME, player));
			}
		}
		
		public boolean isValidPlayer(Session session, Message message)
		{
			//Will check that the username and sessionId match to what we expect
			boolean valid = playerSessions.containsKey(message.getPlayer());
			if (!valid)
			{
				sendMessageOut(session, new SingleStringMessage(Message.ID.ERROR, message.getPlayer(), "Bad username/sessionId. Ignoring message."));
				return false;
			}
			return true;
		}
		
		public void addPlayer(Session session, HandshakeMessage message, LobbyState lobbyState)
		{
			playerSessions.put(message.getPlayer(), session);
			sendMessageOut(session, new LobbyStateMessage(message.getPlayer(), lobbyState));
		}
		
		public void removePlayer(Session session)
		{
			PlayerMeta player = getPlayerFromSession(session);
			playerSessions.remove(player);
			controller.removePlayerFromGame(player);
		}
		
		public Set<PlayerMeta> getConnectedPlayers()
		{
			return playerSessions.keySet();
		}
		
		private PlayerMeta getPlayerFromSession(Session session)
		{
			for (PlayerMeta player : playerSessions.keySet()){
				if (playerSessions.get(player).getId().equals(session.getId()))
				{
					return player;
				}
			}
			return null;
		}
	}
}
