package com.jeffrpowell.sevenwonders;

import java.util.Map;

public class HandshakeMessage extends Message{

	public HandshakeMessage(PlayerMeta player)
	{
		super(ID.HANDSHAKE, player);
	}
	
	public static HandshakeMessage fromMap(Map<String, Object> map)
	{
		PlayerMeta player = PlayerMeta.fromMap((Map<String, Object>)map.get("player"));
		return new HandshakeMessage(player);
	}
}
